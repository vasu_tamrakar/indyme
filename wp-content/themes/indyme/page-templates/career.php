<?php

/*

  Template Name: Career

 */

get_header();

?>

<style>

    .cbb_box{background: none;height: 440px;}

    #header{ background-color: rgba(0, 0, 0, 0.7);height: 160px;}

    .desktop_header {background: #93b924;}

    .headerbottom {top: 85px;}

    .comman_bnr_box{height: 700px;background-size: 100% auto;background-position: top center;}



    /* carousel */

    .media-carousel 

    {

      margin-bottom: 0;

      padding: 0 10px 30px;

      margin-top: 30px;

      overflow: hidden;

    }

    .media-carousel  .carousel-inner {

        overflow: visible;

    }

    /* Previous button  */

    .media-carousel .carousel-control.left 

    {

      left: -12px;

      background-image: none;

      background: none repeat scroll 0 0 #222222;

      border: 4px solid #FFFFFF;

      border-radius: 23px 23px 23px 23px;

      height: 40px;

      width : 40px;

      margin-top: 30px

    }

    /* Next button  */

    .media-carousel .carousel-control.right 

    {

      right: -12px !important;

      background-image: none;

      background: none repeat scroll 0 0 #222222;

      border: 4px solid #FFFFFF;

      border-radius: 23px 23px 23px 23px;

      height: 40px;

      width : 40px;

      margin-top: 30px

    }

    /* Changes the position of the indicators */

    .media-carousel .carousel-indicators 

    {

      right: 50%;

      top: auto;

      bottom: 0px;

      margin-right: -19px;

    }

    .media-carousel a.carousel-control {

        margin: 0 !important;

        top: 38%;

    }

    .padding0{padding: 0;}

    

    /* End carousel */

    @media only screen and (max-width: 768px) {.comman_bnr_box, .cbb_box {height: 400px;}}

    @media only screen and (max-width: 767px) {

      #header{ height: auto;}

      .headerbottom {top: 5px;}

    }

    @media only screen and (max-width: 568px) {

      .comman_bnr_box, .cbb_box {height: 315px;}

    }

    @media only screen and (max-width: 480px) {

      .comman_bnr_box, .cbb_box {height: 262px;}

    }

    @media only screen and (max-width: 360px) {

      .comman_bnr_box, .cbb_box {height: 205px;}

    }

    @media only screen and (max-width: 320px) {

      .comman_bnr_box, .cbb_box {height: 172px;}

    }

</style>
<?php if (have_posts()){ 
  while(have_posts()) { 
    the_post(); ?> 
<div class="comman_bnr_box" style=" background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>');">

    <div class="cbb_box">

        <div class="cbb_contant">

        </div>

    </div>

</div>

<div class="allcomman_box about_us ">

    <div class="tbspace container">
      <?php the_content(); ?>
    </div>

    <div class="container">

         <div class='row'>

            <div class='col-md-12 padding0'>

              <div class="carousel slide media-carousel ss" id="media">

                <div class="carousel-inner">

                    <?php

                                    global $wpdb;

                                    $argss = array(

                                        'post_type' => 'career-slider',

                                        'post_per_page' => -1,

                                        'post_status' => 'publish',

                                    );

                                    wp_reset_query();

                                    query_posts($argss);

                                    $arr=array();

                                    

                                    ?>

                                 <?php if (have_posts()) : 

                                        while (have_posts()) : the_post(); 

                                          $arr[]=$post->ID;

                                        endwhile;

                                       endif;

                                 $cont=count($arr)-1;

                                   for($i=0;$i<=$cont;$i++){

                                       ?>

                                            <div class="item">

                                              <div class="row">

                                                <div class="col-md-4">

                                                  <a href="#"><img class="img-responsive center-block" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($arr[$i])) ?>"></a>

                                                </div>          

                                                <div class="col-md-4">

                                                  <a href="#"><img class="img-responsive center-block" src="<?php if($i>=$cont) { echo wp_get_attachment_url(get_post_thumbnail_id($arr[1]));}  else { echo wp_get_attachment_url(get_post_thumbnail_id($arr[$i+1])); } ?>"></a>

                                                </div>

                                                <div class="col-md-4">

                                                  <a href="#"><img class="img-responsive center-block" src="<?php if($i>=$cont) { echo wp_get_attachment_url(get_post_thumbnail_id($arr[2]));}  else { echo wp_get_attachment_url(get_post_thumbnail_id($arr[$i+2])); } ?>"></a>

                                                </div>        

                                              </div>

                                            </div>

                                <?php $i=$i+2; } ?>


                <a data-slide="prev" href="#media" class="left carousel-control"></a>

                <a data-slide="next" href="#media" class="right carousel-control"></a>

              </div>                          

            </div>

          </div>

    </div>

    <div class="thum-grid container">

        <div class="row">

            <div class="col-lg-12">

                <h2 class="page_heading">Benefits</h2>

            </div>
            <?php 
            $args = array('post_type' => 'benefits',
                          'order_by' => 'date',
                          'order' => 'DESC',
                          'posts_per_page' => -1
                          );
            $query = new WP_Query($args);
            if ($query->have_posts()):
              while ($query->have_posts()){
               $query->the_post();
               ?>

            <div class="col-lg-3 col-md-4 col-xs-6 thumb">

                <div class=" thumbnail">
                 <?php if(get_field('png_file_link')){ ?>
                    <div class="icon_tm">

                        <div class="icon_img"><img src="<?php echo get_field('png_file_link'); ?>" alt="Image"></div>

                    </div>
                 <?php } ?>
                <div class="text-center">

                        <h4><?php the_title(); ?></h4>

                    </div>

                </div>

            </div>
            <?php } endif; ?>
<!--
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">

                <div class=" thumbnail">

                    <div class="icon_tm">

                        <div class="icon_img"><object type="image/svg+xml" data="<?php echo bloginfo('template_url') ?>/fonts/Finance-16.svg" class="img-responsive center-block"></object></div>

                    </div>

                    <div class="text-center">

                        <h4>401k</h4>

                    </div>

                </div>

            </div>

            <div class="col-lg-3 col-md-4 col-xs-6 thumb">

                <div class=" thumbnail">

                    <div class="icon_tm">

                        <div class="icon_img"><object type="image/svg+xml" data="<?php echo bloginfo('template_url') ?>/fonts/Food_Beverages-18.svg" class="img-responsive center-block"></object></div>

                    </div>

                    <div class="text-center">

                        <h4>Paid Time Off</h4>

                    </div>

                </div>

            </div>

            <div class="col-lg-3 col-md-4 col-xs-6 thumb">

                <div class=" thumbnail">

                    <div class="icon_tm">

                        <div class="icon_img"><object type="image/svg+xml" data="<?php echo bloginfo('template_url') ?>/fonts/Education_Science-02.svg" class="img-responsive center-block"></object></div>

                    </div>

                    <div class="text-center">

                        <h4>Tuition Reimbursement</h4>

                    </div>

                </div>

            </div>

            <div class="col-lg-3 col-md-4 col-xs-6 thumb">

                <div class=" thumbnail">

                    <div class="icon_tm">

                        <div class="icon_img"><object type="image/svg+xml" data="<?php echo bloginfo('template_url') ?>/fonts/Faces_Users-04.svg" class="img-responsive center-block"></object></div>

                    </div>

                    <div class="text-center">

                        <h4>Collaborative culture</h4>

                    </div>

                </div>

            </div>

            <div class="col-lg-3 col-md-4 col-xs-6 thumb">

                <div class=" thumbnail">

                    <div class="icon_tm">

                        <div class="icon_img"><object type="image/svg+xml" data="<?php echo bloginfo('template_url') ?>/fonts/Party-03.svg" class="img-responsive center-block"></object></div>

                    </div>

                    <div class="text-center">

                        <h4>Fun company events</h4>

                    </div>

                </div>

            </div>

            <div class="col-lg-3 col-md-4 col-xs-6 thumb">

                <div class=" thumbnail">

                    <div class="icon_tm">

                        <div class="icon_img"><object type="image/svg+xml" data="<?php echo bloginfo('template_url') ?>/fonts/Food_Sweets-35.svg" class="img-responsive center-block"></object></div>

                    </div>

                    <div class="text-center">

                        <h4>Donut Days</h4>

                    </div>

                </div>

            </div>

            <div class="col-lg-3 col-md-4 col-xs-6 thumb">

                <div class=" thumbnail">

                    <div class="icon_tm">

                        <div class="icon_img"><object type="image/svg+xml" data="<?php echo bloginfo('template_url') ?>/fonts/Clothes-12.svg" class="img-responsive center-block"></object></div>

                    </div>

                    <div class="text-center">

                        <h4>Comfy clothes policy</h4>

                    </div>

                </div>

            </div>-->

        </div>

    </div>

        <?php

            global $wpdb;

            $args = array(

                'post_type' => 'career',

                'post_per_page' => 10,

                'post_status' => 'publish',

            );

            wp_reset_query();

            query_posts($args);

            

        if (have_posts()){ ?>

            <div class="latest_news tbspace container">

                <h2 class="page_heading">Current Openings</h2>

                <ul>

                    <?php if (have_posts()): while (have_posts()): the_post(); ?>

                         <li><a href="<?php echo get_permalink(); ?>"><span><i class="fa fa-angle-right"></i></span> <span><?php the_title(); ?></span></a></li>

                   <?php endwhile; endif; ?>

                </ul>

            </div>

       <?php } ?>

</div>
<?php } } ?>
<?php get_footer(); ?>



<script>

   $("#media .carousel-inner div:first").addClass("active");

   // $("#media div:first").addClass("active");



    $(document).ready(function() {

      $('#media').carousel({

        pause: true,

        interval: false,

      });

    });

</script>