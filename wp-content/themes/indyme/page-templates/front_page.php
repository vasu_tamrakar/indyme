<?php
/**
 * Template Name: Front Page
 */
get_header();
?>

<div class="section_1">
    <div class="banner_box">
        <div class="bnr_contant_box container">
            <div class="s_contant ">
                <h2>Make your retail stores smarter</h2>
                <p>The world’s leading in-store awareness and response company.</p>
                <a href="#request" class="primary btn btn-success scroll_btn">REQUEST A DEMO </a>
                <a href="<?php bloginfo('url'); ?>/solutions" class="btn btn-success transparent_btn">LEARN MORE</a>
            </div>
        </div>
    </div>
    <div class="bnr_video" style="background: #000;">
        <video width="100%" height="100%" loop autoplay id="video" style="opacity: 0.8;">
            <source src="<?php echo bloginfo('template_url') ?>/video/indyme_video.mp4" type="video/mp4">
            <source src="<?php echo bloginfo('template_url') ?>/video/indyme_video.ogg" type="video/ogg">
        </video>
    </div>
</div>
<div class="section_2">
    <div class="vedio_box">
        <video width="100%" id="video2" class="scroll_div_video">
            <source src="<?php echo bloginfo('template_url') ?>/video/Indyme_Intro_Video.mp4" type="video/mp4">
            <source src="<?php echo bloginfo('template_url') ?>/video/Indyme_Intro_Video.ogg" type="video/ogg">
        </video>
    </div>
    <div id="play-video" class="vedeo_overlayer scroll_btn_video">
        <div class="player-wrap text-center">
            <div class="row">
                <div class="play-icon">
                    <h3>WHAT IS SMART RESPONSE?</h3>
                    <div><a href="#video2" class="scroll_btn_video"><img src="<?php echo bloginfo('template_url') ?>/images/play-btn.png" class="img-responsive" ></a></div>
                    <h3>Watch the video</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section_3">
    <div class="container"><h2>Explore how our solutions can work for your stores</h2></div>
    <div class="thumnail_slider">
        <div class="row">
            <div class="col-md-12">
                <div class="well">
                    <div id="myCarousel" class="carousel fdi-Carousel slide" data-ride="carousel" data-pause="false"> 
                        <!-- Carousel items -->
                        <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                            <div class="carousel-inner onebyone-carosel">
                                <?php
                                global $wpdb;
                                $argss = array(
                                    'post_type' => 'store',
                                    'post_per_page' => -1,
                                    'post_status' => 'publish',
                                    'order_by' => 'DATE',
                                    'order' => 'ASC'
                                );
                                wp_reset_query();
                                query_posts($argss);
                                ?>
                                <?php $f = 0;
                                    if (have_posts()) : while (have_posts()) :$f++;
                                the_post(); ?>
                                        <div class="item ">
                                            <div class="col-md-4">
                                                <div class="thum_box"> <a href="#"><img src="<?php
                                                    if (has_post_thumbnail()) {
                                                        echo wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                                    }
                                                    ?>" class="img-responsive center-block"></a>
                                                    <div class="text-center">
                                                        <h4><?php the_title(); ?></h4>
                                                        <div class="thum_mbox">
                                                            <p><?php the_field('tagline'); ?></p>
                                                        </div>
                                                        <a href="<?php the_field('modal-url'); ?>" class="l_more">LEARN MORE</a> </div>
                                                </div>
                                            </div>
                                            <?php if ($f > 3) { ?>
                                                <style>
                                                    .carousel-control_d{
                                                        display:block !important;
                                                    }
                                                </style>
                                        <?php } ?>
                                        </div>
    <?php
    endwhile;
endif;
?>

                            </div>
                            <a class="left carousel-control_d" href="#eventCarousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a> <a class="right carousel-control_d" href="#eventCarousel" data-slide="next"><i class="fa fa-chevron-right"></i></a> </div>
                        <!--/carousel-inner--> 
                    </div>
                    <!--/myCarousel--> 
                </div>
                <!--/well--> 
            </div>
        </div>
    </div>
</div>
<div class="section_5">
    <div class="container">
        <div class="section_5_box col-md-6"></div>
        <div class="section_5_box col-md-6">
            <div class="box_contant">
                <h3>Smart Sense: <br/>
                    loss prevention<br/>
                    with customer<br/>
                    service benefits</h3>
                <p>Our sensing technology helps you differentiate suspicious behavior from normal shopping behavior and tailor responses based on threat levels.</p>
                <div class="indybtn">
                    <a href="<?php bloginfo('url'); ?>/contact-us" class="primary btn btn-success">REQUEST A DEMO </a>
                    <a href="<?php bloginfo('url'); ?>/solutions" class="btn btn-success transparent_btn">LEARN MORE</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="case_study" class="section_6">
    <h2>Successful Case Studies</h2>
    <div class="thumnail_slider">
        <div class="">
            <div class="span12">
                <div class="well" style="background: #f1f1f1;">
                    <div id="myCarousel2" class="carousel fdi-Carousel slide" data-ride="carousel" data-pause="true"> 
                        <!-- Carousel items -->
                        <div class="carousel fdi-Carousel slide" id="eventCarousel2" data-interval="0">
                            <div class="carousel-inner onebyone-carosel">
                                <?php
                                global $wpdb;
                                $argss = array(
                                    'post_type' => 'case-studies',
                                    'post_per_page' => -1,
                                    'post_status' => 'publish',
                                    'order_by' => 'DATE',
                                    'order' => 'ASC'
                                );
                                wp_reset_query();
                                query_posts($argss);
                                ?>
                                    <?php $f = 0;
                                        if (have_posts()) : while (have_posts()) :$f++;
                                               the_post(); ?>
                                        <div class="item">
                                            <div class="col-md-4">
                                                <div class="thum_box"> <a href="#"><img src="<?php
                                                                                        if (has_post_thumbnail()) {
                                                                                            echo wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                                                                        }
                                                                                        ?>" class="img-responsive center-block"></a>
                                                    <div class="text-center">
                                                        <div class="thum_mbox2">
                                                            <h4><?php the_field('tagline'); ?></h4>
                                                        </div>
                                                        <a href="<?php echo get_permalink(); ?>" class="l_more">LEARN MORE</a> </div>
                                                </div>
                                            </div>
                                        <?php if ($f > 3) { ?>
                                                <style>
                                                    .carousel-control_d{
                                                        display:block !important;
                                                    }
                                                </style>
        <?php } ?>
                                        </div>
    <?php
    endwhile;
endif;
?>
                            </div>
                            <a class="left carousel-control_d" href="#eventCarousel2" data-slide="prev"><i class="fa fa-chevron-left"></i></a> <a class="right carousel-control_d" href="#eventCarousel2" data-slide="next"><i class="fa fa-chevron-right"></i></a> </div>
                        <!--/carousel-inner--> 
                    </div>
                    <!--/myCarousel--> 
                </div>
                <!--/well--> 
            </div>
        </div>
    </div>
</div>
<div id="request" class="section_4 scroll_div">
    <div class="container">
        <h2>WE'RE READY TO HELP YOU: <span style="color:#a8c716;">Improve the customer experience and increase sales with smart response.</span></h2>
        <div class="home_form">
<?php echo do_shortcode('[contact-form-7 id="57" title="Home Contact us"]'); ?>
        </div>
    </div>
</div>
<div class="section_7">
    <div class="container">
        <h2>Trusted by Leading Retailers</h2>
           <?php
                global $wpdb;
                $argss = array(
                        'post_type' => 'retailer',
                        'posts_per_page' => -1,
                        'post_status' => 'publish',
                        'order_by' => 'DATE',
                        'order' => 'ASC'
                        );
                        wp_reset_query();
                        query_posts($argss);
                        ?>
                        <?php 
                            $f = 0;
                            if (have_posts()) : while (have_posts()) :$f++;
                            the_post(); 
                         ?>
        <div class="col-xs-3 col-sm-3 col-md-3">

        <!-- <img src="<?php //echo bloginfo('template_url') ?>/images/partners_logo/albertsons-logo.jpg" class="img-responsive center-block" /> -->
        <img src="<?php
                        if (has_post_thumbnail()) {
                        echo wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                        }?>" class="img-responsive center-block" />
        </div>

        <?php
            endwhile;
        endif;
        ?>
    <!--     <div class="col-xs-3 col-sm-3 col-md-3"><img src="<?php //echo bloginfo('template_url') ?>/images/partners_logo/bevmo-logo.jpg" class="img-responsive center-block" /></div>
        <div class="col-xs-3 col-sm-3 col-md-3"><img src="<?php //echo bloginfo('template_url') ?>/images/partners_logo/containerstorelogo.jpg" class="img-responsive center-block" /></div>
        <div class="col-xs-3 col-sm-3 col-md-3"><img src="<?php //echo bloginfo('template_url') ?>/images/partners_logo/dicks.jpg" class="img-responsive center-block" /></div>
        <div class="col-xs-3 col-sm-3 col-md-3"><img src="<?php //echo bloginfo('template_url') ?>/images/partners_logo/ikea-logo.jpg" class="img-responsive center-block" /></div>
        <div class="col-xs-3 col-sm-3 col-md-3"><img src="<?php //echo bloginfo('template_url') ?>/images/partners_logo/logo-lowes-gray.jpg" class="img-responsive center-block" /></div>
        <div class="col-xs-3 col-sm-3 col-md-3"><img src="<?php //echo bloginfo('template_url') ?>/images/partners_logo/michaels-Logo.jpg" class="img-responsive center-block" /></div>
        <div class="col-xs-3 col-sm-3 col-md-3"><img src="<?php //echo bloginfo('template_url') ?>/images/partners_logo/stop-and-shop-logo.jpg" class="img-responsive center-block" /></div> -->
    </div>
</div>
<?php get_footer(); ?>
<script>
    $("#eventCarousel .carousel-inner div:first").addClass("active");
    $("#eventCarousel2 .carousel-inner div:first").addClass("active");
</script>
 <script src="/wp-content/themes/indyme/js/jquery.modal.js" type="text/javascript" charset="utf-8"></script>
 <script type="text/javascript" charset="utf-8">
  $(function() {

    function log_modal_event(event, modal) {
      if(typeof console != 'undefined' && console.log) console.log("[event] " + event.type);
    };

    $(document).on($.modal.BEFORE_BLOCK, log_modal_event);
    $(document).on($.modal.BLOCK, log_modal_event);
    $(document).on($.modal.BEFORE_OPEN, log_modal_event);
    $(document).on($.modal.OPEN, log_modal_event);
    $(document).on($.modal.BEFORE_CLOSE, log_modal_event);
    $(document).on($.modal.CLOSE, log_modal_event);
    $(document).on($.modal.AJAX_SEND, log_modal_event);
    $(document).on($.modal.AJAX_SUCCESS, log_modal_event);
    $(document).on($.modal.AJAX_COMPLETE, log_modal_event);

    $('#manual-ajax').click(function(event) {
      event.preventDefault();
      $.get(this.href, function(html) {
        $(html).appendTo('body').modal();
      });
    });

  });
</script>