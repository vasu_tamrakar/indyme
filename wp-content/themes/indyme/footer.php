<?php 
wp_reset_query();
if(is_page( 'spvm' )){ ?>
<?php } else { ?>
<div class="section_8 ">
    <div class="container">
      <div class="col-md-4">
        <div class="section_8_box">
        <div class="left"><i class="fa fa-phone"></i></div>
        <div class="right"><strong>+1.858.268.0717</strong><br/>
          We’re here to answer any questions you may have. Give us a call! </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="section_8_box">
          <div class="left"><i class="call-center"><img src="<?php echo bloginfo('template_url') ?>/images/people.png" class="center-block"> </i></div>
          <div class="right"><strong>+1.858.268.0717</strong><br/>
            <a href="mailto:service@indyme.com">service@indyme.com</a><br/>
            <strong>Need technical assistance?</strong><br/>
            Hours: 6:00am to 5:00pm PST</div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="section_8_box reseller">
            <?php if(is_page( 'Contact Us' )){ ?>
          	<a href="#find_reseller" class="reseller_box scrool_btn"><i class="fa fa-search"></i> Find a reseller </a>
            <?php } else { ?>
            <a href="<?php echo bloginfo('url'); ?>/contact-us?find_reseller" class="reseller_box scrool_btn"><i class="fa fa-search"></i> Find a reseller </a>
        	<?php    } ?>
        </div>
      </div>
    </div>
</div>
<?php } ?>
<footer id="footer">
<div class="container">
  	<div class="footer_top">
    	<div class="ft_logo">
        	<div class="logobox">
              <div style="width: 185px;"><a href="<?php bloginfo('url'); ?>"><img src="<?php echo bloginfo('template_url') ?>/images/white-logo.png" /></a></div>
            </div>
        </div>
        <div class="ft_menu">
            <p><a href="<?php bloginfo('url'); ?>/legal-licensing">Legal & Licensing</a> | <a href="<?php bloginfo('url'); ?>/privacy-policy">Privacy Policy</a></p>
        </div>
    </div>
    <div class="footer_bottom">
    	  <div class="fmenu_box"><?php wp_nav_menu( array( 'theme_location' => 'Footermenu1' ) ); ?></div>
        <div class="fmenu_box"><?php wp_nav_menu( array( 'theme_location' => 'Footermenu2' ) ); ?></div>
        <div class="fmenu_box"><?php wp_nav_menu( array( 'theme_location' => 'Footermenu3' ) ); ?></div>
        <div class="fmenu_box"><?php wp_nav_menu( array( 'theme_location' => 'Footermenu4' ) ); ?></div>
        <div class="fmenu_box"><?php wp_nav_menu( array( 'theme_location' => 'Footermenu5' ) ); ?></div>
        <div class="fmenu_box"><?php wp_nav_menu( array( 'theme_location' => 'Footermenu6' ) ); ?></div>
    </div>
    </div>
  </footer>
</div>
<?php wp_footer(); ?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo bloginfo('template_url') ?>/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $("#toggle ").click(function(){
        $(".mainmenu").toggle();
    });
});

$(document).ready(function() {
	$('#play-video').click(function(){
		$("#play-video").hide();
    	document.getElementById('video2').play();
	});
	$('video').click(function(){
		document.getElementById('video2').pause();
		$("#play-video").show();
	   	
	});
	$('.vedio_box').click(function(){
		document.getElementById('video2').pause();
		$("#play-video").show();
	});
});

</script>
<script>
$(document).ready(function () {
    $('#myCarousel').carousel({
        interval: false,
        autoplay:false
    });
	$('#myCarousel2').carousel({
        interval: false,
        autoplay:false
    });
	$('#myCarousel3').carousel({
        interval: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay:false
    });
    $('.fdi-Carousel .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length > 0) {
            next.next().children(':first-child').clone().appendTo($(this));
        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
});



 $(document).ready(function() {
      $('.slideToggle1').click(function() {
        var next = $(this).next('.slideTogglebox1');
        next.toggle();
        $('.slideTogglebox1').not(next).hide();
      });
    }); 
	
</script>
<script>
   $(document).ready(function(){
	   $(window).bind('scroll', function() {
	   var navHeight = $( window ).height() -  $( window ).height();
			 if ($(window).scrollTop() > navHeight) {
				 $('.desktop_header, .mobile_header').addClass('fixed');
			 }
			 else {
				 $('.desktop_header, .mobile_header').removeClass('fixed');
			 }
		});
	});
	
	
</script>
<!-- How it works Js -->
<script type="text/javascript">
     
     $(".hiwToggle").click(function(){      
      var data_id = $(this).attr('data-id')-1;
      $(".hiwTogglebox").slideUp('slow');  
      if($(this).hasClass('active'))
      {  
         $(".hiwToggle").removeClass('active'); 
         $(".hiwTogglebox").slideUp('slow');
         $('div.hiwToggle i.arr_up_dwn').removeClass('fa-angle-up').addClass('fa-angle-down'); 
      } 
      else{
         
        $(".hiwToggle").removeClass('active'); 
        $(this).addClass('active');
        $('div.hiwToggle i.arr_up_dwn').removeClass('fa-angle-up').addClass('fa-angle-down'); 
        $('div.hiwToggle.active i.arr_up_dwn').removeClass('fa-angle-down').addClass('fa-angle-up'); 
        $(".hiwTogglebox:eq("+data_id+")").slideDown('slow');
      }  

  });

    $(".scroll_btn").click(function() {
      $('html,body').animate({scrollTop: $(".scroll_div").offset().top - 75},'slow');
    });
    $(".scroll_btn_video").click(function() {
      $('html,body').animate({scrollTop: $(".scroll_div_video").offset().top - 75},'slow');
    });
    $("#footer ul li a").click(function() {
      $('html,body').animate({scrollTop: $("#case_study").offset().top - 75},'slow');
    });
     $(".section_8_box").click(function() {
      $('html,body').animate({scrollTop: $("#wpsl-wrap").offset().top - 75},'slow');
    });
   
    
    
</script>
<script>
$("#menu-item-334 a").attr("href", "<?php echo bloginfo('url'); ?>/?case_study");
$(document).ready(function(){
    var str = window.location.href;
    var res = str.split("?");
    var links = res[1];
    if(links == 'request'){
        $('li.re_demo a').trigger('click',500);
    }
    if(links == 'case_study'){
        $('li#menu-item-334 a').trigger('click',500);
    }
    });
</script>

<script>
//$("#menu-item-334 a").attr("href", "<?php echo bloginfo('url'); ?>/?find_reseller");
$(document).ready(function(){
    var str = window.location.href;
    var res = str.split("?");
    var links = res[1];
    if(links == 'find_reseller'){
        $('a').trigger('click',900);
    }
    });
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53860650-1', 'auto');
  ga('require', 'linkid');
  ga('send', 'pageview');



</script>


</body>
</html>