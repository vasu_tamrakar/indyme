<?php

/*

  Template Name: How It Work

 */

get_header();

?>

<link href="<?php echo bloginfo('template_url') ?>/css/feature.presenter.1.6.css" rel="stylesheet" type="text/css">

    <style>

        .hov_circle, .clk_circle, .hov_circle:hover path.i_color, 

        .hov_circle:hover .i_color > *, .clk_circle:hover path, 

        .clk_circle:hover rect, .clk_circle:hover polygon, .clk_circle:hover circle{

            -webkit-transition: all 0.3s ease-in-out 0s;

            -moz-transition: all 0.3s ease-in-out 0s;

            -o-transition: all 0.3s ease-in-out 0s;

            -ms-transition: all 0.3s ease-in-out 0s;

            transition: all 0.3s ease-in-out 0s;

        }

        .hov_circle, .clk_circle{cursor: pointer;}

        .hov_circle:hover path.bg_color {

            fill: rgb(167, 198, 22);

        }

        .hov_circle:hover path.i_color, .hov_circle:hover .i_color > *{

            stroke: rgb(255, 255, 255);



        }

        .clk_circle:hover path, .clk_circle:hover rect, .clk_circle:hover polygon {

            fill: rgb(58, 167, 187);



        }

        .clk_circle:hover circle {

            stroke: rgb(58, 167, 187);

        }

        .tooltip {

            padding: 30px 5px;

            display: block;

            background: #000;

            margin-top: -15px !important;

            margin-bottom: 0px;

            border-radius: 3px;

            -webkit-border-radius: 3px;

            -ms-border-radius: 3px;

            -o-border-radius: 3px;

            font-size: 14px;



        }



        .tooltip .tooltiptext {padding: 20px;top: -40px !important;}

        .tooltip::after {

            content: '';

            display: block;  

            position: absolute;

            left: 45%;

            top: 100%;

            width: 0;

            height: 0;

            border-top: 10px solid #000000;

            border-left: 10px solid transparent;

            border-right: 10px solid transparent;

            border-bottom: 0 solid transparent;

        }

        #my_mike:hover{

            stroke: #fff !important;

            fill:none;

        }

    </style>

    <div class="comman_bnr_box" style=" background-image: url('../wp-content/uploads/2016/07/howitworks-hero.jpg');">

        <div class="cbb_box">

            <div class="cbb_contant">

                <div class="container">

                    <h2><?php echo get_field('banner_heading'); ?></h2>

                    <p><?php echo get_field('banner_subheading'); ?></p>

                    <!--<h2>The sole purpose of products is to drive result</h2>

                              <p>30 years of experience, thousands of install, a dedicated team of experts who live and breathe retail</p>--> 

                </div>

            </div>

        </div>

    </div>

    <div class="tbspace allcomman_box how_work ">

        <div class="container">

            <h2 class="page_heading">The Smart Response System</h2>

            <p>Smart Response gives your store a voice. It helps you create engagement opportunities by outfitting the store with sensor networks that detect any in-store event and trigger tailored notifications.</p>

        </div>

        <div class="margin30" style="background-color:#fff;">

            <div class="view_circle">

                <h4 class="pull-left">Any Event</h4>

                <h4 class="pull-right">Any Response</h4>

                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                     viewBox="0 0 841.9 595.3" enable-background="new 0 0 841.9 595.3" xml:space="preserve">

                    <line display="none" fill="none" stroke="#A8C716" stroke-miterlimit="10" x1="553.1" y1="335.7" x2="604.9" y2="361.9"/>

                    <circle fill="none" stroke="#DEDEDE" stroke-width="4" stroke-miterlimit="10" cx="422" cy="272.7" r="251.2"/>

                    <g id="g1" class="hov_circle">

                        <path class="bg_color" fill="#FFFFFF" d="M354.4,55c2.2,24.3-15.6,45.8-39.9,48.1c-24.3,2.2-45.8-15.6-48.1-39.9c-2.3-24.3,15.6-45.8,39.9-48.1

                              S352.1,30.6,354.4,55"/>

                        <circle fill="none" stroke="#DEDEDE" stroke-width="4" stroke-miterlimit="10" cx="310.4" cy="59.1" r="44.2"/>

                        <g class="i_color">

                            <g class="i_color">



                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M317.2,45.1c-3-2.4-7.3-3.9-12-3.9c-9.2,0-16.7,5.7-16.7,12.7c0,2.3,0.8,4.5,2.3,6.4l-2.2,7.5l9.3-2.5c2.2,0.8,4.7,1.3,7.4,1.3

                                      c4,0,7.7-1.1,10.6-2.9"/>



                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M321.3,44.6c7.3,0,13.2,4.5,13.2,10c0,1.8-0.7,3.6-1.8,5.1l1.8,5.9l-7.4-2c-1.8,0.7-3.7,1-5.8,1c-7.3,0-13.2-4.5-13.2-10

                                      S314,44.6,321.3,44.6z"/>

                            </g>

                            <g class="i_color">

                                <circle fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="321.6" cy="54.4" r="0.7"/>





                                <circle fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="325.5" cy="54.4" r="0.7"/>





                                <circle fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="317.8" cy="54.4" r="0.7"/>





                                <circle fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="304.7" cy="53.5" r="0.7"/>





                                <circle fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="300.9" cy="53.5" r="0.7"/>

                            </g>

                        </g>

                    </g>

                    <g>

                        <line fill="none" x1="325.3" y1="86.7" x2="351.9" y2="127.5"/>

                        <line fill="none" stroke="#DEDEDE" stroke-width="4" stroke-miterlimit="10" x1="331.1" y1="97" x2="353.7" y2="131.6"/>

                        <polygon fill="#DEDEDE" points="360.6,142.2 346.8,130.9 356.2,124.7 		"/>

                        <polygon fill="none" stroke="#DEDEDE" stroke-miterlimit="10" points="360.6,142.2 346.8,130.9 356.2,124.7 		"/>

                    </g>

                    <g id="g2" class="hov_circle">

                        <path class="bg_color" fill="#FFFFFF" d="M252.8,129.5c2.7,24.7-15.6,47-41,49.8c-25.4,2.8-48.1-15-50.8-39.7c-2.7-24.7,15.6-47,41-49.8

                              C227.3,87,250.1,104.7,252.8,129.5"/>



                        <ellipse transform="matrix(-0.994 0.1095 -0.1095 -0.994 425.3994 247.7513)" fill="none" stroke="#DEDEDE" stroke-width="4" stroke-miterlimit="10" cx="205.9" cy="135.6" rx="46.2" ry="45"/>

                        <g class="i_color">

                            <g class="i_color">

                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M222.4,139.5v-10.4c0-1.4-1.1-2.5-2.5-2.5h-18.5v-3.7c0-1.4-1.1-2.5-2.5-2.5h-7.7c-1.4,0-2.5,1.1-2.5,2.5v16.6"/>

                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M214.1,123.6c1,0,1.8-0.8,1.8-1.8v-1.5c0-1-0.8-1.8-1.8-1.8h-5.4c-1,0-1.8,0.8-1.8,1.8v1.5c0,1,0.8,1.8,1.8,1.8H214.1z"/>

                                <line fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="209" y1="121.1" x2="213.7" y2="121.1"/>

                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M224.4,144.3c0,1.5-1.2,2.8-2.8,2.8h-32.1c-1.5,0-2.8-1.3-2.8-2.8v-2.2c0-1.3,0.8-2.3,2-2.7c0.3-0.1,0.5-0.1,0.8-0.1h32.1

                                      c0.3,0,0.6,0,0.8,0.1c1.1,0.3,2,1.4,2,2.7L224.4,144.3L224.4,144.3z"/>



                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M209.2,130.4c0,0.1-0.1,0.2-0.2,0.2h-0.6c-0.1,0-0.2-0.1-0.2-0.2v-0.6c0-0.1,0.1-0.2,0.2-0.2h0.6c0.1,0,0.2,0.1,0.2,0.2

                                      V130.4z"/>

                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M212.2,130.4c0,0.1-0.1,0.2-0.2,0.2h-0.6c-0.1,0-0.2-0.1-0.2-0.2v-0.6c0-0.1,0.1-0.2,0.2-0.2h0.6c0.1,0,0.2,0.1,0.2,0.2

                                      V130.4z"/>



                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M215.2,130.4c0,0.1-0.1,0.2-0.2,0.2h-0.6c-0.1,0-0.2-0.1-0.2-0.2v-0.6c0-0.1,0.1-0.2,0.2-0.2h0.6c0.1,0,0.2,0.1,0.2,0.2

                                      V130.4z"/>

                            </g>

                            <g class="i_color">



                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M209.2,133.3c0,0.1-0.1,0.2-0.2,0.2h-0.6c-0.1,0-0.2-0.1-0.2-0.2v-0.6c0-0.1,0.1-0.2,0.2-0.2h0.6c0.1,0,0.2,0.1,0.2,0.2

                                      V133.3z"/>



                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M212.2,133.3c0,0.1-0.1,0.2-0.2,0.2h-0.6c-0.1,0-0.2-0.1-0.2-0.2v-0.6c0-0.1,0.1-0.2,0.2-0.2h0.6c0.1,0,0.2,0.1,0.2,0.2

                                      V133.3z"/>



                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M215.2,133.3c0,0.1-0.1,0.2-0.2,0.2h-0.6c-0.1,0-0.2-0.1-0.2-0.2v-0.6c0-0.1,0.1-0.2,0.2-0.2h0.6c0.1,0,0.2,0.1,0.2,0.2

                                      V133.3z"/>

                            </g>

                            <g class="i_color">



                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M209.2,136.2c0,0.1-0.1,0.2-0.2,0.2h-0.6c-0.1,0-0.2-0.1-0.2-0.2v-0.6c0-0.1,0.1-0.2,0.2-0.2h0.6c0.1,0,0.2,0.1,0.2,0.2

                                      V136.2z"/>



                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M212.2,136.2c0,0.1-0.1,0.2-0.2,0.2h-0.6c-0.1,0-0.2-0.1-0.2-0.2v-0.6c0-0.1,0.1-0.2,0.2-0.2h0.6c0.1,0,0.2,0.1,0.2,0.2

                                      V136.2z"/>



                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M215.2,136.2c0,0.1-0.1,0.2-0.2,0.2h-0.6c-0.1,0-0.2-0.1-0.2-0.2v-0.6c0-0.1,0.1-0.2,0.2-0.2h0.6c0.1,0,0.2,0.1,0.2,0.2

                                      V136.2z"/>

                            </g>

                            <g class="i_color">



                                <circle fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="205.5" cy="143.2" r="1.2"/>

                            </g>

                        </g>



                    </g>

                    <line fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="199" y1="126.6" x2="191.2" y2="126.6"/>



                    <polyline fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="

                              192.7,126.5 192.7,122.7 197.6,122.7 197.6,126.5 		"/>



                    <line fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="211.4" y1="123.6" x2="211.4" y2="126.6"/>

                    </g>

                    </g>

                    <g>

                        <line fill="none" x1="236.9" y1="150.2" x2="285" y2="184.8"/>

                        <line fill="none" stroke="#DEDEDE" stroke-width="4" stroke-miterlimit="10" x1="246.5" y1="158.4" x2="293" y2="191.5"/>

                        <polygon fill="#DEDEDE" points="297.4,194.4 280.4,188.9 286.9,179.8 			"/>

                        <polygon fill="none" stroke="#DEDEDE" stroke-miterlimit="10" points="298.3,195.5 281.3,190 287.8,180.9 			"/>

                    </g>

                    <g id="g3" class="hov_circle">

                        <path class="bg_color" fill="#FFFFFF" d="M217.7,254c0,24.4-19.8,44.2-44.2,44.2s-44.2-19.8-44.2-44.2s19.8-44.2,44.2-44.2S217.7,229.6,217.7,254"/>

                        <circle fill="none" stroke="#DEDEDE" stroke-width="4" stroke-miterlimit="10" cx="173.5" cy="254" r="44.2"/>

                        <g class="i_color">





                            <line fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="150.3" y1="263.7" x2="154.9" y2="268.3"/>



                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M180.5,242.4l-4.1-4.1c-1.2-1.2-2.8-1.6-4-0.4l-20.8,20.8c-0.7,0.6-0.8,1.3-0.5,2c-0.6,0.9-1,2.1-0.7,3l-0.1,0.1

                                  c-0.8,0.8-0.4,1.5,0.2,2.2l2.2,2.2c0.7,0.7,1.3,1,2.2,0.2l0.1-0.1c0.9,0.3,2.2,0,3.1-0.4c0.7,0.2,1.4,0.2,2-0.5l20.8-20.8

                                  C182.2,245.3,181.8,243.6,180.5,242.4z"/>



                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M169.6,257.9l2.1,2.1c0.1,0.1,0.2,0.1,0.3,0.2v0.1c0,1.6,1.3,3,3,3c0.8,0,1.6-0.3,2.1-0.9c0.1,0,0.1,0,0.2,0h7v2.9

                                  c0,0.6,0.9,1.1,2,1.1s2-0.5,2-1.1v-9.8c0-0.6-0.9-1.1-2-1.1s-2,0.5-2,1.1v2.9h-6.9c-0.5-0.7-1.4-1.1-2.3-1.1c-0.1,0-0.2,0-0.3,0

                                  c0-0.1-0.1-0.1-0.1-0.2l-2.2-2.2"/>

                        </g>

                    </g>

                    <g>

                        <line fill="none" x1="218.9" y1="252.9" x2="267.6" y2="252.6"/>

                        <line fill="none" stroke="#DEDEDE" stroke-width="4" stroke-miterlimit="10" x1="218.9" y1="252.9" x2="267.6" y2="252.6"/>

                        <polygon fill="#DEDEDE" points="279.6,253.3 262.6,258.8 262.6,247.6 		"/>

                        <polygon fill="none" stroke="#DEDEDE" stroke-miterlimit="10" points="279.6,253.3 262.6,258.8 262.6,247.6 		"/>

                    </g>

                    <g id="g4" class="hov_circle">

                        <path class="bg_color" fill="#FFFFFF" d="M234.1,379.9c0,24.4-19.8,44.2-44.2,44.2s-44.2-19.8-44.2-44.2s19.8-44.2,44.2-44.2

                              S234.1,355.5,234.1,379.9"/>

                        <circle fill="none" stroke="#DEDEDE" stroke-width="4" stroke-miterlimit="10" cx="189.8" cy="379.9" r="44.2"/>

                        <g class="i_color">

                            <path fill="#A8C716" d="M201.9,392.5h-22.7c-0.2,0-0.3-0.1-0.3-0.3v-10.8c0-6.4,5.2-11.7,11.7-11.7c6.4,0,11.7,5.2,11.7,11.7v10.8

                                  C202.2,392.4,202,392.5,201.9,392.5z M179.4,391.9h22.1v-10.5c0-6.1-4.9-11.1-11.1-11.1c-6.1,0-11.1,4.9-11.1,11.1l0,0v10.5H179.4

                                  z"/>

                            <path fill="#A8C716" d="M190.5,389.8h-8.7c-0.2,0-0.3-0.1-0.3-0.3v-8.1c0-5,4-9,9-9c0.2,0,0.3,0.1,0.3,0.3v16.8

                                  C190.8,389.7,190.7,389.8,190.5,389.8z M182.1,389.2h8.1V373c-4.5,0.2-8.1,3.9-8.1,8.4V389.2L182.1,389.2z"/>

                            <path fill="#A8C716" d="M199.2,381.7c-0.2,0-0.3-0.1-0.3-0.3c0-3.8-2.5-7.1-6.2-8.1c-0.2,0-0.3-0.2-0.2-0.4c0-0.2,0.2-0.3,0.4-0.2

                                  c3.9,1.1,6.6,4.6,6.6,8.7C199.5,381.6,199.3,381.7,199.2,381.7L199.2,381.7z"/>

                            <path fill="#A8C716" d="M184.5,381.7c-0.2,0-0.3-0.1-0.3-0.3c0-2.4,1.3-4.6,3.5-5.6c0.1-0.1,0.3,0,0.4,0.1c0.1,0.1,0,0.3-0.1,0.4

                                  l0,0c-1.9,1-3.2,2.9-3.2,5.1C184.8,381.6,184.7,381.7,184.5,381.7z"/>

                            <path fill="#A8C716" d="M202.7,395.5h-24.5c-0.2,0-0.3-0.1-0.3-0.3v-3c0-0.2,0.1-0.3,0.3-0.3h24.5c0.2,0,0.3,0.1,0.3,0.3v3

                                  C203.1,395.3,202.9,395.5,202.7,395.5z M178.6,394.9h23.9v-2.3h-23.9V394.9z"/>

                            <path fill="#A8C716" d="M190.5,368.7c-0.2,0-0.3-0.1-0.3-0.3v-8.2c0-0.2,0.1-0.3,0.3-0.3c0.2,0,0.3,0.1,0.3,0.3v8.2

                                  C190.8,368.6,190.7,368.7,190.5,368.7z"/>

                            <path fill="#A8C716" d="M181.3,372.5c-0.1,0-0.2,0-0.2-0.1l-5.8-5.8c-0.1-0.1-0.1-0.3,0-0.4s0.3-0.1,0.4,0l5.8,5.8

                                  c0.1,0.1,0.1,0.3,0,0.4C181.4,372.5,181.4,372.5,181.3,372.5z"/>

                            <path fill="#A8C716" d="M177.5,381.7h-8.2c-0.2,0-0.3-0.1-0.3-0.3s0.1-0.3,0.3-0.3h8.2c0.2,0,0.3,0.1,0.3,0.3

                                  C177.8,381.6,177.6,381.7,177.5,381.7z"/>

                            <path fill="#A8C716" d="M203.5,381.7c-0.2,0-0.3-0.1-0.3-0.3s0.1-0.3,0.3-0.3h8.2c0.2,0,0.3,0.1,0.3,0.3c0,0.2-0.1,0.3-0.3,0.3

                                  H203.5z"/>

                            <path fill="#A8C716" d="M199.7,372.5c-0.2,0-0.3-0.1-0.3-0.3c0-0.1,0-0.2,0.1-0.2l5.8-5.8c0.1-0.1,0.3-0.1,0.4,0

                                  c0.1,0.1,0.1,0.3,0,0.4l-5.8,5.8C199.9,372.5,199.8,372.5,199.7,372.5z"/>

                        </g>

                    </g>

                    <g>

                        <line fill="none" x1="229.3" y1="361.9" x2="281.1" y2="335.7"/>

                        <line fill="none" stroke="#DEDEDE" stroke-width="4" stroke-miterlimit="10" x1="229.3" y1="361.9" x2="281.1" y2="335.7"/>

                        <polygon fill="#DEDEDE" points="299.6,327.4 284.2,342 278.5,330.7 		"/>

                        <polygon fill="none" stroke="#DEDEDE" stroke-miterlimit="10" points="299.6,327.4 284.2,342 278.5,330.7 		"/>

                    </g>

                    <g id="g5" class="hov_circle">

                        <path class="bg_color" fill="#FFFFFF" d="M320.6,477.6c-5.9,23.7-29.9,38.1-53.6,32.2c-23.7-5.9-38.1-29.9-32.2-53.6c5.9-23.7,29.9-38.1,53.6-32.2

                              C312.1,430,326.5,454,320.6,477.6"/>

                        <circle fill="none" stroke="#DEDEDE" stroke-width="4" stroke-miterlimit="10" cx="277.7" cy="467" r="44.2"/>

                        <g class="i_color">

                            <path fill="#A8C716" d="M294,487.9c-0.8,0-1.4-0.2-1.9-0.7l-15-14.9c-0.2-0.2-0.2-0.5,0-0.7c0.2-0.2,0.5-0.2,0.7,0l15,14.9

                                  c0.6,0.6,2.2,0.6,3.4-0.6c1.2-1.2,1-2.7,0.4-3.3l-15-14.9c-0.2-0.2-0.2-0.5,0-0.7c0.2-0.2,0.5-0.2,0.7,0l15,14.9

                                  c1,1,1.1,3.1-0.4,4.6C296,487.5,294.9,487.9,294,487.9z"/>

                            <path fill="#A8C716" d="M276,471c-0.1,0-0.2,0-0.3-0.1l-6.4-6.3c-1.1,0.4-4.6,1.4-7.1-1c-2.8-2.8-2.2-6.5-2.1-6.7

                                  c0-0.1,0.1-0.2,0.1-0.2l0.9-0.9c0.2-0.2,0.5-0.2,0.7,0l2.9,2.9l2.8,0.7l1.6-1.6l-0.7-2.4l-3.1-3.1c-0.2-0.2-0.2-0.5,0-0.7

                                  l1.3-1.3c0.1-0.1,0.2-0.1,0.3-0.1c0.2,0,4-0.5,6.6,2.1c2.3,2.3,1.4,6.3,1.1,7.4l6.2,6.2c0.2,0.2,0.2,0.5,0,0.7s-0.5,0.2-0.7,0

                                  l-6.4-6.4c-0.1-0.1-0.2-0.3-0.1-0.5c0,0,1.5-4.5-0.8-6.7c-2-2-5-1.9-5.7-1.9l-0.8,0.8l2.8,2.8c0.1,0.1,0.1,0.1,0.1,0.2l0.8,2.8

                                  c0,0.2,0,0.4-0.1,0.5l-2,2c-0.1,0.1-0.3,0.2-0.5,0.1l-3.2-0.8c-0.1,0-0.2-0.1-0.2-0.1l-2.7-2.7l-0.5,0.5

                                  c-0.1,0.7-0.3,3.5,1.9,5.7c2.4,2.4,6.3,0.7,6.3,0.7c0.2-0.1,0.4,0,0.5,0.1l6.6,6.6C276.3,470.5,276.3,470.8,276,471

                                  C276.2,470.9,276.1,471,276,471z"/>

                            <path fill="#A8C716" d="M294.5,486.5c-1.1,0-2.1-0.9-2.1-2.1c0-1.1,0.9-2.1,2.1-2.1c1.2,0,2.1,0.9,2.1,2.1

                                  C296.5,485.6,295.6,486.5,294.5,486.5z M294.5,483.3c-0.6,0-1.1,0.5-1.1,1.1c0,0.6,0.5,1.1,1.1,1.1c0.6,0,1.1-0.5,1.1-1.1

                                  C295.6,483.8,295.1,483.3,294.5,483.3z"/>

                            <path fill="#A8C716" d="M277.3,469.7c-0.1,0-0.2,0-0.3-0.1l-3.7-3.7c-0.4-0.4-0.7-0.9-0.8-1.5c-0.1-0.6,0.1-1.1,0.4-1.5

                                  c0.7-0.7,2-0.6,2.9,0.4l3.7,3.7c0.2,0.2,0.2,0.5,0,0.7c-0.2,0.2-0.5,0.2-0.7,0l-3.7-3.7c-0.5-0.5-1.3-0.7-1.6-0.4

                                  c-0.2,0.2-0.2,0.5-0.1,0.6c0.1,0.3,0.2,0.7,0.5,1l3.7,3.7c0.2,0.2,0.2,0.5,0,0.7C277.6,469.6,277.5,469.7,277.3,469.7z"/>



                            <path fill="#A8C716" d="M290.8,482.3c-0.6,0-1.3-0.3-1.8-0.8L278.5,471c-0.2-0.2-0.2-0.5,0-0.7c0.2-0.2,0.5-0.2,0.7,0l10.5,10.5

                                  c0.5,0.5,1.3,0.7,1.6,0.4c0.2-0.2,0.2-0.5,0.1-0.6c-0.1-0.3-0.2-0.7-0.5-1l-10.5-10.5c-0.2-0.2-0.2-0.5,0-0.7s0.5-0.2,0.7,0

                                  l10.5,10.5c0.9,0.9,1.1,2.2,0.4,2.9C291.6,482.2,291.2,482.3,290.8,482.3z"/>

                            <path fill="#A8C716" d="M275.6,474.2c-0.1,0-0.2,0-0.3-0.1c-0.2-0.2-0.2-0.5,0-0.7l18-18c0-0.1,0.1-0.1,0.1-0.1

                                  c0.1-0.1,0.3-0.2,0.5-0.1l0,0c0,0,1.2,0.2,3.3-1.9c0-0.1-0.1-0.2-0.4-0.5l-1.6-1.6c-0.2-0.2-0.4-0.3-0.5-0.4

                                  c-2.1,2.1-1.9,3.2-1.9,3.2c0,0.2,0,0.4-0.1,0.5c0,0.1-0.1,0.1-0.1,0.1l-18,18c-0.2,0.2-0.5,0.2-0.7,0c-0.2-0.2-0.2-0.5,0-0.7

                                  l17.9-17.9c0-0.6,0.2-1.9,2.2-3.9c0.2-0.2,0.4-0.2,0.6-0.2l0,0c0.4,0,0.8,0.2,1.2,0.7l1.6,1.6c1,1,0.7,1.5,0.4,1.8

                                  c-2,2-3.3,2.2-3.9,2.2L276,474.1C275.9,474.2,275.8,474.2,275.6,474.2z"/>

                            <path fill="#A8C716" d="M264.7,488.1c-0.6,0-1.1-0.2-1.6-0.6l-2.5-2.5c-0.4-0.4-0.6-1-0.6-1.6c0-0.6,0.2-1.2,0.7-1.6l10.3-10.3

                                  c0.8-0.8,2.3-0.8,3.1,0l2.5,2.5c0.4,0.4,0.6,1,0.6,1.6s-0.2,1.1-0.7,1.6l-10.3,10.3C265.8,487.8,265.3,488.1,264.7,488.1z

                                  M272.5,471.8c-0.3,0-0.7,0.1-0.9,0.4l-10.3,10.3c-0.2,0.2-0.4,0.6-0.4,0.9c0,0.3,0.1,0.6,0.4,0.9l2.5,2.5

                                  c0.5,0.5,1.3,0.5,1.8,0l10.3-10.3c0.2-0.2,0.4-0.6,0.4-0.9s-0.1-0.6-0.4-0.9l-2.5-2.5C273.1,471.9,272.8,471.8,272.5,471.8z"/>

                        </g>

                    </g>

                    <g>

                        <line fill="none" x1="304.3" y1="436.5" x2="337.3" y2="395.6"/>

                        <line fill="none" stroke="#DEDEDE" stroke-width="4" stroke-miterlimit="10" x1="304.3" y1="434.1" x2="337.3" y2="395.6"/>

                        <polygon fill="#DEDEDE" points="349.6,381.7 342.3,399.6 333.4,392.3 		"/>

                        <polygon fill="none" stroke="#DEDEDE" stroke-miterlimit="10" points="349.6,381.7 342.3,399.6 333.4,392.3 		"/>

                    </g>

                    <g id="g6" class="hov_circle">

                        <path class="bg_color" fill="#FFFFFF" d="M482.5,513.8c0,38.7-31.4,70.1-70.1,70.1s-70.1-31.4-70.1-70.1s31.4-70.1,70.1-70.1

                              S482.5,475.1,482.5,513.8"/>

                        <circle fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" cx="412.4" cy="513.8" r="69.5"/>

                        <g class="i_color">

                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M387.9,519.9c0-1.4-1.2-2.5-2.5-2.5h-2.8c-1.4,0-2.5,1.1-2.5,2.5l0,0v18.5h7.9L387.9,519.9L387.9,519.9z"/>



                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M400.4,513.7c-0.1-1.3-1.2-2.4-2.5-2.4h-2.8c-1.4,0-2.5,1.1-2.5,2.4l0,0v24.7h7.9L400.4,513.7L400.4,513.7z"/>



                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M412.9,505c0-1.4-1.1-2.5-2.5-2.5h-2.8c-1.4,0-2.5,1.1-2.5,2.5l0,0l0,0l0,0l0,0v33.4h7.9L412.9,505L412.9,505z"/>



                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M425.5,498.9C425.5,498.9,425.5,498.8,425.5,498.9L425.5,498.9L425.5,498.9c0-1.4-1.2-2.5-2.5-2.5h-2.8c-1.4,0-2.5,1.1-2.5,2.5

                                  l0,0v39.6h7.9L425.5,498.9L425.5,498.9z"/>



                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M438,492.1L438,492.1L438,492.1L438,492.1c-0.1-1.4-1.2-2.5-2.5-2.5h-2.8c-1.4,0-2.5,1.1-2.5,2.5l0,0v46.4h7.9L438,492.1

                                  L438,492.1z"/>

                        </g>

                    </g>

                    <g>

                        <line fill="none" x1="465.6" y1="414.3" x2="456.7" y2="386.8"/>

                        <polygon fill="#A8C716" points="409.5,424.6 420.9,424.7 414.4,442.3 		"/>

                        <line fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" x1="414.5" y1="424.7" x2="413.4" y2="382.8"/>

                        <polygon fill="none" stroke="#A8C716" stroke-miterlimit="10" points="409.5,424.6 420.9,424.7 414.4,442.3 		"/>

                    </g>                    

                    <g id="g7" class="hov_circle">

                        <path class="bg_color" fill="#FFFFFF" d="M601.8,489.9c-8.7,24.1-35.3,36.6-59.4,27.9c-24.1-8.7-36.6-35.3-27.9-59.4c8.7-24.1,35.3-36.6,59.4-27.9

                              C598,439.2,610.5,465.8,601.8,489.9"/>

                        <circle fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" cx="558.2" cy="474.1" r="46.4"/>

                        <g class="i_color">

                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M577.3,484.7l-17.2-27.8c-0.2-0.3-0.6-0.5-0.9-0.5c-0.4,0-0.7,0.2-0.9,0.5L541,484.7c-0.2,0.3-0.2,0.8,0,1.1

                                  c0.2,0.3,0.6,0.6,1,0.6h34.5l0,0c0.6,0,1.1-0.5,1.1-1.1C577.5,485.1,577.4,484.9,577.3,484.7z"/>



                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M557.9,475l-0.4-5.3c-0.1-1-0.1-1.8-0.1-2.2c0-0.6,0.2-1.1,0.5-1.4c0.3-0.3,0.8-0.5,1.3-0.5c0.6,0,1.1,0.2,1.3,0.7

                                  c0.2,0.4,0.3,1.1,0.3,1.9c0,0.5,0,1-0.1,1.5l-0.5,5.5c-0.1,0.7-0.2,1.2-0.3,1.5c-0.2,0.3-0.5,0.5-0.8,0.5

                                  c-0.4,0-0.7-0.2-0.8-0.5C558.1,476.2,557.9,475.7,557.9,475z M559.1,482.3c-0.5,0-0.9-0.1-1.2-0.4s-0.5-0.7-0.5-1.2

                                  c0-0.5,0.2-0.9,0.5-1.2s0.7-0.5,1.2-0.5s0.9,0.2,1.2,0.5c0.3,0.3,0.5,0.7,0.5,1.2c0,0.5-0.2,0.9-0.5,1.2

                                  C559.9,482.2,559.6,482.3,559.1,482.3z"/>

                        </g>

                    </g>

                    <g>

                        <line fill="none" x1="499.7" y1="372.2" x2="527.1" y2="414.5"/>

                        <line fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" x1="498" y1="369.4" x2="527.1" y2="414.5"/>

                        <polygon fill="#A8C716" points="536.1,429.8 522.3,417.6 531,412 	"/>

                        <polygon fill="none" stroke="#A8C716" stroke-miterlimit="10" points="536.1,429.8 522.3,417.6 531,412 	"/>

                    </g>

                    <g id="g8" class="hov_circle">

                        <path class="bg_color" fill="#FFFFFF" d="M600,380.4c0-24.4,19.8-44.2,44.2-44.2c24.4,0,44.2,19.8,44.2,44.2c0,24.4-19.8,44.2-44.2,44.2

                              C619.8,424.6,600,404.8,600,380.4"/>

                        <circle fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" cx="644.4" cy="379.9" r="44.2"/>

                        <g class="i_color">



                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M652.9,387.1v7c0,2.3-1.2,3.4-3.4,3.4h-11.7c-2.3,0-3.4-1.2-3.4-3.4v-28.2c0-2.3,1.2-3.4,3.4-3.4h11.7c2.3,0,3.4,1.2,3.4,3.4

                                  v5.6"/>



                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M643.8,395.7h-0.4c-0.1,0-0.2,0-0.2-0.1c-0.1-0.1-0.1-0.2-0.1-0.2V395c0-0.2,0.2-0.3,0.3-0.3h0.4c0.1,0,0.2,0,0.2,0.1

                                  c0.1,0.1,0.1,0.2,0.1,0.2v0.4C644.2,395.5,644,395.7,643.8,395.7z"/>



                            <line fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="641.8" y1="364.6" x2="645.4" y2="364.6"/>

                            <line fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="635.2" y1="366.9" x2="651.9" y2="366.9"/>



                            <line fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="651.9" y1="392.5" x2="635.2" y2="392.5"/>

                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M656.9,371.1c-6,0-10.8,3.7-10.8,8.2c0,1.5,0.5,2.9,1.5,4.1l-1.4,4.8l6-1.6c1.4,0.5,3,0.8,4.7,0.8c6,0,10.8-3.7,10.8-8.2

                                  C667.7,374.7,662.9,371.1,656.9,371.1z"/>



                            <circle fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="653.3" cy="379.3" r="0.4"/>





                            <circle fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="656.9" cy="379.3" r="0.4"/>

                            <circle fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="660.5" cy="379.3" r="0.4"/>

                        </g>

                    </g>

                    <g>

                        <line fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" x1="586.4" y1="353.6" x2="534.6" y2="327.4"/>

                        <polygon fill="#A8C716" stroke="#A8C716" stroke-miterlimit="10" points="583.8,358.6 589.5,347.3 604.9,361.9 		"/>

                        <polygon fill="#A8C716" stroke="#A8C716" stroke-miterlimit="10" points="583.8,358.6 589.5,347.3 604.9,361.9 		"/>

                    </g>

                    <g id="g9" class="hov_circle">

                        <path class="bg_color" fill="#FFFFFF" d="M714.6,250.6c0,25.6-20.8,46.4-46.4,46.4c-25.6,0-46.4-20.8-46.4-46.4s20.8-46.4,46.4-46.4

                              C693.8,204.2,714.6,225,714.6,250.6"/>

                        <circle fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" cx="668.5" cy="251.2" r="46.4"/>

                        <g class="i_color">



                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M668.8,237.7c-0.4-0.2-0.8-0.2-1.1,0l-9.9,6.4H654c-0.6,0-1.1,0.5-1.1,1.1v9.7c0,0.6,0.5,1.1,1.1,1.1h3.7l9.9,6.4

                                  c0.2,0.1,0.4,0.2,0.6,0.2c0.2,0,0.4,0,0.5-0.1c0.4-0.2,0.6-0.6,0.6-1v-22.9C669.3,238.3,669.1,237.9,668.8,237.7z"/>

                            <g class="i_color" id="my_mike">

                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M671.6,257.9c2.5-1,4.2-4,4.2-7.6c0-3.1-1.4-6-3.5-7.2"/>

                            </g>

                            <g class="i_color" id="my_mike">

                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M673.6,261.1c3.6-1.4,6-5.8,6-10.9c0-4.5-2-8.6-5-10.4"/>

                            </g>

                            <g class="i_color" id="my_mike">

                                <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                      M677,236.9c4,2.3,6.5,7.6,6.5,13.3c0,6.4-3.2,12.2-7.9,14c-0.1,0-0.2,0.1-0.3,0.1"/>

                            </g>

                        </g>

                    </g>

                    <!--                    <g id="g9" class="hov_circle" data-original-title="" title="">

                                            <path class="bg_color" fill="#FFFFFF" d="M714.9,264.2c0,25.6-20.8,46.4-46.4,46.4c-25.6,0-46.4-20.8-46.4-46.4c0-25.6,20.8-46.4,46.4-46.4

                                                  C694.1,217.8,714.9,238.6,714.9,264.2"></path>

                                            <circle fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" cx="668.5" cy="264.2" r="46.4"></circle>

                                            <path class="i_color" fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                                  M668.8,237.7c-0.4-0.2-0.8-0.2-1.1,0l-9.9,6.4H654c-0.6,0-1.1,0.5-1.1,1.1v9.7c0,0.6,0.5,1.1,1.1,1.1h3.7l9.9,6.4

                                                  c0.2,0.1,0.4,0.2,0.6,0.2c0.2,0,0.4,0,0.5-0.1c0.4-0.2,0.6-0.6,0.6-1v-22.9C669.3,238.3,669.1,237.9,668.8,237.7z"/>

                                            <path class="i_color" fill="#AFCB43" d="M686.8,264.1c-0.4,7-3.5,11.7-9.2,14.8c-1,0.5-1.9,1.2-2.7-0.1c-0.8-1.3,0.3-1.9,1.2-2.3

                                                  c9.5-4.5,10.7-19.3-0.1-24.9c-1-0.5-2-1.2-1.1-2.4c0.8-1.1,1.8-0.4,2.8,0.1C683.7,252.7,686.8,257.8,686.8,264.1"></path>

                                            <path class="i_color" fill="#AFCB43" d="M681.1,265.7c-0.1,2.9-2.8,7.1-5.8,8.4c-0.9,0.4-1.9,0.6-2.5-0.3c-0.7-1,0.3-1.6,0.9-2.2

                                                  c6.2-5.4,6.2-9.5,0-14.8c-0.7-0.6-1.7-1-1-2.2c0.7-1.2,1.7-0.8,2.7-0.2C679.3,256.7,681.3,260.1,681.1,265.7"></path>

                                            <path class="i_color" fill="#AFCB43" d="M675.5,264c0.1,1.7-0.5,3-1.5,4c-0.8,0.8-1.7,1.9-2.9,1c-1.4-1-0.4-1.9,0.4-2.8c1.5-1.4,1.4-2.9,0-4.3

                                                  c-0.7-0.7-1.8-1.4-0.6-2.5c1.2-1.1,2.2-0.1,3,0.7C675,261.2,675.8,262.5,675.5,264"></path>

                                        </g>

                                        <g>

                                            <line fill="none" x1="549" y1="249.3" x2="599.4" y2="251.1"/>

                                            <line fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" x1="549" y1="249.3" x2="599.4" y2="251.1"/>

                                            <polygon fill="#A8C716" points="616.9,252.5 599.1,256.9 599.5,246.5 		"/>

                                            <polygon fill="none" stroke="#A8C716" stroke-miterlimit="10" points="616.9,252.5 599.1,256.9 599.5,246.5 		"/>

                                        </g>-->



                    <g id="g10" class="hov_circle">

                        <path class="bg_color" fill="#FFFFFF" d="M678.5,140.3c-2.8,25.4-25.8,43.8-51.2,41s-43.8-25.8-41-51.2c2.8-25.4,25.8-43.8,51.2-41

                              C663,91.9,681.3,114.9,678.5,140.3"/>

                        <circle fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" cx="631.8" cy="133.5" r="46.4"/>

                        <g class="i_color">

                            <path fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="

                                  M643.6,148.2c0,1.2-0.7,1.8-1.8,1.8H623c-1.2,0-1.8-0.7-1.8-1.8v-25.8c0-1.2,0.7-1.8,1.8-1.8h18.8c1.2,0,1.8,0.7,1.8,1.8

                                  V148.2z"/>



                            <rect x="623.1" y="122.9" fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" width="18.6" height="24.1"/>

                            <line fill="none" stroke="#A8C716" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="632.2" y1="148.5" x2="632.6" y2="148.5"/>

                        </g>

                    </g>

                    <g>

                        <line fill="none" x1="529" y1="201.3" x2="572.7" y2="171.6"/>

                        <line fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" x1="529" y1="201.3" x2="572.7" y2="171.6"/>

                        <polygon fill="#A8C716" points="588.4,161.9 575.9,176.4 570.1,167.7 		"/>

                        <polygon fill="none" stroke="#A8C716" stroke-miterlimit="10" points="588.4,161.9 575.9,176.4 570.1,167.7 		"/>

                    </g>



                    <g id="g11" class="hov_circle">

                        <path class="bg_color" fill="#FFFFFF" d="M571,56.1c0,25.6-20.8,46.4-46.4,46.4c-25.6,0-46.4-20.8-46.4-46.4S499,9.7,524.6,9.7

                              C550.2,9.7,571,30.5,571,56.1"/>

                        <circle fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" cx="524.8" cy="56.1" r="46.4"/>

                        <g class="i_color">

                            <path fill="#A8C716" d="M532.8,40.9v-4.6c0-1.2-1-2.1-2.2-2.1c-1.2,0-2.1,0.9-2.1,2.1V40h-9.3c-1.9,0-2.8,0.7-3.4,1.6l0,0l0,0

                                  c0,0.1-0.1,0.1-0.1,0.2v0.1c0,0.1-0.1,0.1-0.1,0.2v0.1c0,0.1,0,0.1-0.1,0.2v0.1v0.1l0,0c-0.1,0.4-0.2,0.9-0.2,1.4v26

                                  c0,2.5,1.4,3.9,3.9,3.9H530c2.5,0,3.9-1.4,3.9-3.9V43.9C534,42.6,533.6,41.6,532.8,40.9z M532.5,70c0,1.7-0.7,2.5-2.5,2.5h-10.8

                                  c-1.7,0-2.5-0.8-2.5-2.5V48.4l0,0v-4.5c0-0.7,0.2-1.3,0.5-1.7l0,0c0.3-0.4,0.9-0.7,2-0.7h10c0.4,0,0.7-0.3,0.7-0.7v-4.4

                                  c0-0.4,0.3-0.7,0.7-0.7s0.7,0.3,0.7,0.7v4.9c0,0.2,0.1,0.5,0.3,0.6c0.6,0.4,0.8,1,0.8,2.1L532.5,70L532.5,70z"/>

                            <path fill="#A8C716" d="M522.6,43.9h4c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7h-4c-0.4,0-0.7,0.3-0.7,0.7

                                  C521.9,43.5,522.2,43.9,522.6,43.9z"/>

                            <path fill="#A8C716" d="M520.3,57.4h-2.5c-0.5,0-0.9,0.4-0.9,0.9l0,0v2.3c0,0.5,0.4,0.9,0.9,0.9h2.5c0.5,0,0.9-0.4,0.9-0.9v-2.3

                                  C521.2,57.8,520.8,57.4,520.3,57.4L520.3,57.4z M519.7,59.9h-1.3v-1.1h1.3V59.9z"/>

                            <path fill="#A8C716" d="M520.3,62.1h-2.5c-0.5,0-0.9,0.4-0.9,0.9l0,0v2.3c0,0.5,0.4,0.9,0.9,0.9h2.5c0.5,0,0.9-0.4,0.9-0.9V63

                                  C521.2,62.5,520.8,62.1,520.3,62.1L520.3,62.1z M519.7,64.7h-1.3v-1.1h1.3V64.7z"/>

                            <path fill="#A8C716" d="M520.3,67h-2.5c-0.5,0-0.9,0.4-0.9,0.9v2.3c0,0.5,0.4,0.9,0.9,0.9l0,0h2.5c0.5,0,0.9-0.4,0.9-0.9l0,0v-2.3

                                  C521.2,67.4,520.8,67,520.3,67z M519.7,69.5h-1.3v-1.1h1.3V69.5L519.7,69.5z"/>

                            <path fill="#A8C716" d="M531.4,67h-2.5c-0.5,0-0.9,0.4-0.9,0.9v2.3c0,0.5,0.4,0.9,0.9,0.9l0,0h2.5c0.5,0,0.9-0.4,0.9-0.9l0,0v-2.3

                                  C532.3,67.4,531.9,67,531.4,67z M530.8,69.5h-1.3v-1.1h1.3V69.5L530.8,69.5z"/>

                            <path fill="#A8C716" d="M526.3,57.7c-0.1-0.2-0.4-0.4-0.6-0.4h-2.4c-0.5,0-0.9,0.4-0.9,0.9l0,0v2.3c0,0.5,0.4,0.9,0.9,0.9h2.5

                                  c0.5,0,0.9-0.4,0.9-0.9v-2.1C526.7,58.1,526.6,57.9,526.3,57.7z M525.2,59.9h-1.3v-1.1h1.3V59.9L525.2,59.9z"/>

                            <path fill="#A8C716" d="M525.8,62.1h-2.5c-0.5,0-0.9,0.4-0.9,0.9l0,0v2.3c0,0.5,0.4,0.9,0.9,0.9h2.5c0.5,0,0.9-0.4,0.9-0.9V63

                                  C526.7,62.5,526.3,62.1,525.8,62.1L525.8,62.1z M525.2,64.7h-1.3v-1.1h1.3V64.7L525.2,64.7z"/>

                            <path fill="#A8C716" d="M531.4,62.1h-2.5c-0.5,0-0.9,0.4-0.9,0.9l0,0v2.3c0,0.5,0.4,0.9,0.9,0.9h2.5c0.5,0,0.9-0.4,0.9-0.9V63

                                  C532.3,62.5,531.9,62.1,531.4,62.1L531.4,62.1z M530.8,64.7h-1.3v-1.1h1.3V64.7L530.8,64.7z"/>

                            <path fill="#A8C716" d="M531.4,57.4h-2.5c-0.5,0-0.9,0.4-0.9,0.9l0,0v2.3c0,0.5,0.4,0.9,0.9,0.9h2.5c0.5,0,0.9-0.4,0.9-0.9v-2.3

                                  C532.3,57.8,531.9,57.4,531.4,57.4L531.4,57.4z M530.8,60h-1.3v-1.1h1.3V60L530.8,60z"/>

                            <path fill="#A8C716" d="M525.8,67h-2.5c-0.5,0-0.9,0.4-0.9,0.9v2.3c0,0.5,0.4,0.9,0.9,0.9l0,0h2.5c0.5,0,0.9-0.4,0.9-0.9l0,0v-2.3

                                  C526.7,67.4,526.3,67,525.8,67z M525.2,69.5h-1.3v-1.1h1.3V69.5L525.2,69.5z"/>

                            <path fill="#A8C716" d="M517.8,56.5h13.5c0.4,0,0.7-0.3,0.7-0.7V45.7c0-0.4-0.3-0.7-0.7-0.7h-13.5c-0.4,0-0.7,0.3-0.7,0.7v10.1

                                  C517.1,56.2,517.4,56.5,517.8,56.5z M519.9,46.4h10.7V55h-12v-8.6H519.9z"/>

                        </g>

                    </g>

                    <g>

                        <line fill="none" x1="470.8" y1="151.4" x2="492.4" y2="112.2"/>

                        <line fill="none" stroke="#A8C716" stroke-width="4" stroke-miterlimit="10" x1="470.8" y1="151.4" x2="492.4" y2="112.2"/>

                        <polygon fill="#A8C716" points="500.6,98.9 497.5,115 488.4,110 		"/>

                        <polygon fill="none" stroke="#A8C716" stroke-miterlimit="10" points="500.6,98.9 497.5,115 488.4,110 		"/>

                    </g>



                    <g id="mid-circle">

                        <path fill="#A8C73A" d="M475.9,145.5c20.3,9.9,36.9,24.2,50,42.7c1.3,1.9,4.1,6.1,5.7,8.8c11.7,19.5,17.9,40.7,18.9,63.3

                              c0.1,2.3,0.1,7.3,0,10.2c-0.9,25.6-8.5,49.1-22.8,70.3c-1.8,2.7-4.9,6.9-6.9,9.3c-22.7,27.7-51.8,43.8-87.1,48.7

                              c-3.7,0.5-7.5,0.9-11.3,0.9c-2.7,0-7.7,0.1-9.7,0c-4-0.1-8.1-0.5-12.5-0.9c-2.4-0.8-7.3-1.7-7.3-1.7s-15.4-2.6-18.9-4.5

                              c-48-17.9-78.2-51.8-88.2-102.3c-13.3-67.1,27.1-132.3,90.6-152.4c29.7-9.4,59.2-8.5,88.3,2.6C468,142,473.3,144.3,475.9,145.5"/>

                        <path fill="none" d="M396.8,273.4l28.8,19.7L396.8,273.4z"/>

                        <path fill="#FFFFFF" d="M444.8,226.6c-6.9,1.3-51.5-33.6-57.2-36.8c-3.4,1.9-5.7,2-7.4,5c4.8,4.6,48.2,34.9,56,38.5

                              c23.3,6.1,39.3-14.3,53.7-9.6c4.7,0,62.1,41.1,62.1,41.1l7.1-3.5c0,0-58.4-43.1-69.2-44.9C472.4,213,459.5,225.8,444.8,226.6z"/>

                        <circle fill="#FFFFFF" cx="375.5" cy="184.8" r="13.7"/>

                        <path fill="#FFFFFF" d="M490,344.9c-2.4,0.2-16.8-13.4-18.7-14.7c-1.2,0.6-2.9,2.5-3.6,3.5c1.5,1.7,16.2,14.8,18.8,16.3

                              c8.5,2.6,15.6-4.2,19-1.9c1.6,0.2,20.1,17,20.1,17l2.9-4.5c0,0-18.9-16.9-22.5-17.9C499.8,340.6,495.8,345.7,490,344.9z"/>

                        <circle fill="#FFFFFF" cx="464.6" cy="327.4" r="8"/>

                        <path fill="#FFFFFF" d="M516.1,308.1H310.3c-5.5,0-10-4.5-10-10v-30.7c0-5.5,4.5-10,10-10h205.8c5.5,0,10,4.5,10,10v30.7

                              C526.1,303.6,521.6,308.1,516.1,308.1z"/>

                        <path fill="#FFFFFF" d="M469.8,363.7c-0.4-3-20.8-16.2-22.9-18.1c0.3-1.6,2.2-4.2,3.1-5.3c2.5,1.3,22.3,15.1,24.9,17.8

                              c5.5,9.4-0.5,19.8,3.1,23.2c0.7,1.9,26.1,19.2,26.1,19.2l-4.5,4.7c0,0-25.6-17.7-27.8-21.8C467.5,376.6,472.4,370.4,469.8,363.7z"/>

                        <path fill="#FFFFFF" d="M383.9,301c2.1-2.2,1.4-1.8,8.8-1c-5.3,2.7-16.5,17.7-17.4,23.4c-1.1,1.7,1.6,56.2,1,55.8

                              c-2.8-2.4-5.5,0-5.5,0s-1.9-54.1,0-58.3C370.7,318.4,378.3,306.9,383.9,301z"/>

                        <path fill="#FFFFFF" d="M347.7,301c-2.1-2.2-1.4-1.8-8.8-1c5.3,2.7,16.5,18.7,17.4,24.4c1.1,1.7,1.2,54.8,1.2,54.8s2-2.1,5.2,0

                              c0.4,0.5-0.1-54.1-2-58.3C360.9,318.4,353.2,306.9,347.7,301z"/>

                        <circle fill="#FFFFFF" cx="441.7" cy="338.3" r="9.8"/>

                        <circle fill="#FFFFFF" cx="373.6" cy="386.9" r="6.4"/>

                        <circle fill="#FFFFFF" cx="359.6" cy="385.9" r="5.7"/>

                        <path fill="#A8C73A" d="M511.2,304.3H315.5c-5.5,0-10-4.5-10-10v-23.1c0-5.5,4.5-10,10-10h195.7c5.5,0,10,4.5,10,10v23.1

                              C521.2,299.8,516.7,304.3,511.2,304.3z"/>

                        <text transform="matrix(1 0 0 1 311.189 291.4949)" fill="#FFFFFF" font-family="'PTSans-Bold'" font-size="29.998">Smart Response</text>

                    </g>

                    <path fill="#FFFFFF" stroke="#FFFFFF" stroke-width="5" stroke-miterlimit="10" d="M358.7,376.8"/>



                </svg>







            </div>

            <div class="smart_sns">

                <div class="smart_sns_box1" style="background:rgba(21, 21, 21, 1);">

                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                         viewBox="0 0 841.9 595.3" enable-background="new 0 0 841.9 595.3" xml:space="preserve">



                        <path display="none" fill="#FEFEFE" d="M539.4,144.2c0.1,0.7-0.6,0.9-1,1.2c-9.5,7.2-18.9,14.3-28.4,21.4c-0.3,0.3-0.8,0.4-1,1

                              c40.1,17.6,80.2,35.1,120.5,52.7c-0.1-2-0.5-3.7-0.6-5.4c-0.4-5-1.2-10-1.8-14.9c-0.6-5.6-1.4-11.2-2.1-16.8

                              c-1-8.3-2.1-16.5-3.1-24.8c-0.9-7.2-1.9-14.3-2.8-21.5c-0.5-3.8-1-7.5-1.4-11.3c-0.5-4.2-1.1-8.4-1.6-12.6

                              c-0.5-3.8-0.9-7.7-1.4-11.6c-0.4-3.7-0.9-7.3-1.4-11c-0.1-0.4,0.2-1-0.3-1.3c-0.5-0.2-0.8,0.3-1.1,0.6c-8.9,6.7-17.9,13.4-26.8,20.2

                              c-0.4,0.3-0.7,0.8-1.3,0.7c-0.8-0.6-1.6-1.3-2.5-1.9c-12.1-8.5-25.5-14.4-39.5-18.8c-8.8-2.8-17.8-4.9-27-6.4c-5-0.8-10-1.4-15-2.1

                              c-2.9-0.4-5.8-0.5-8.7-0.6c-2.3,0-2.6,0.1-3.6,2.2c-0.2,0.5-0.4,1-0.6,1.5c-1.9,5.6-2.3,11.4-3.2,17.2c-0.7,4.7-1.3,9.4-2.5,14.1

                              c-0.2,0.9-0.1,1.2,0.9,1.4c2.1,0.5,4.2,1.1,6.3,1.7C506.6,125.3,523.9,132.9,539.4,144.2z"/>

                        <path display="none" fill="#FEFEFE" d="M545.1,454c-0.1-0.2-0.2-0.4-0.3-0.6c-2.5-9.1-4.9-18.1-7.4-27.2c-0.8-3.1-1.7-6.2-2.6-9.6

                              c-31.5,30.9-62.8,61.6-94.2,92.4c42.9,11.2,85.5,22.4,128.2,33.6c0.2-0.6,0-1.1-0.1-1.6c-2.2-8-4.4-15.9-6.5-23.9

                              c-0.8-3.1-1.9-6.2-2.4-9.3c5.1-3.2,9.5-7.2,13.6-11.6c8.4-8.9,15.3-18.8,21.7-29.2c11.6-18.8,21.2-38.7,30.1-58.9

                              c0.6-1.4,0.7-2.3-0.3-3.5c-0.9-1.1-1.4-2.4-1.9-3.7c-0.9-2.4-1.8-4.9-2.7-7.3c-1.7-4.3-4.2-8-7.8-11c-0.6-0.5-0.9-0.7-1.4,0.1

                              c-3.5,5.1-7.3,9.9-11.1,14.6c-12.3,15.3-25,30.2-39.2,43.8C555.8,445.8,550.8,450.3,545.1,454z"/>

                        <path display="none" fill="#FEFEFE" d="M227.5,315.9c0.8,6.2,2.3,12.2,4.2,18.1c7.4,22.9,18.8,43.8,32.8,63.3

                              c1.1,1.6,2.5,2.5,4.3,2.8c0.9,0.2,1.8,0.3,2.7,0.4c5.4,0.5,10.8,0.2,16.3-0.2c4.6-0.4,9.2-0.9,13.8-1.4c0.8-0.1,0.9-0.3,0.5-1

                              c-1-1.7-1.9-3.5-2.7-5.3c-5.9-13.1-10.2-26.7-13.6-40.5c-2.1-8.4-4.1-16.8-4.1-25.5c0.4,0,0.8,0,1.2,0.1c8.1,1.6,16.1,3.1,24.2,4.7

                              c4.4,0.9,8.8,1.7,13.4,2.6c-13.8-41.8-27.5-83.4-41.3-125.2c-0.5,0.5-0.8,0.9-1.1,1.2c-22.1,25.5-44.3,50.9-66.4,76.4

                              c-6.3,7.2-12.5,14.4-18.8,21.6c-0.6,0.6-0.9,1,0.3,1.2c6.1,1.1,12.2,2.3,18.3,3.5C216.7,313.7,222.1,314.8,227.5,315.9z"/>

                        <path fill="#474747" d="M185,250.6c-4.9-3.8-9.7-7.6-14.6-11.4c-5.5-4.2-10.9-8.5-16.4-12.7c-1.1-0.8-0.6-1,0.3-1.4

                              c9.8-4,19.6-8,29.5-12.1c34.7-14.2,69.4-28.4,104.1-42.6c0.5-0.2,1-0.4,1.7-0.7c-7.2,48.4-14.3,96.6-21.5,145c-4.1-3.2-8-6.2-12-9.3

                              c-7.2-5.6-14.4-11.2-21.7-16.8c-0.4-0.3-0.8-0.5-1.2-0.7c-0.1-0.2-0.2-0.3-0.4-0.5c-5-3.9-10.1-7.8-15.1-11.7

                              c-7.3-5.7-14.6-11.4-22-17.1C192.3,256.1,188.6,253.4,185,250.6z"/>

                        <path fill="#474747" d="M438.5,541.3c-1.1,3.4-1.6,7-2.4,10.4c-1.9,9-3.8,18-5.7,26.9c-0.1,0.6-0.2,1.2-0.7,1.7

                              c-36.5-32.7-73-65.4-109.7-98.2c46.7-14.5,93.2-29,140-43.5c-0.8,3.8-1.5,7.3-2.3,10.8c-2.2,10.2-4.3,20.4-6.5,30.6

                              c-0.1,0.2,0,0.5-0.1,0.7c-1.2,5.6-2.5,11.3-3.7,16.9c-2.3,11.1-4.7,22.1-7,33.2C439.9,534.3,438.9,537.7,438.5,541.3z"/>

                        <path fill="#474747" d="M618.3,198.8c0.6,0.2,1.1-0.2,1.6-0.4c11.5-4.8,22.9-9.6,34.4-14.4c0.4-0.2,0.9-0.7,1.4-0.3

                              c0.4,0.3,0,0.9,0,1.4c-0.5,4.1-1,8.2-1.5,12.2c-0.6,4.3-1.2,8.5-1.7,12.8c-0.6,4.7-1.1,9.4-1.7,14.1c-0.6,4.2-1.1,8.4-1.6,12.5

                              c-1,8-2,15.9-2.9,23.9c-1.1,9.2-2.3,18.4-3.4,27.6c-0.8,6.2-1.5,12.4-2.4,18.6c-0.8,5.5-1.3,11.1-2.2,16.6c-0.3,1.8-0.4,3.7-0.8,6

                              c-38.5-30-76.9-59.9-115.3-89.9c0.4-0.6,0.9-0.6,1.4-0.8c12.2-5.1,24.3-10.2,36.5-15.3c0.5-0.2,1.3-0.1,1.3-1

                              c0.6,0.3,1.2-0.1,1.7-0.3c5.7-2.3,11.4-4.7,17.1-7.1c10.4-4.3,20.7-8.7,31.1-13.1C613.6,201,616.1,200.4,618.3,198.8z"/>

                        <path fill="#383838" d="M438.5,541.3c0.4-3.6,1.3-7,2.1-10.5c2.3-11.1,4.6-22.1,7-33.2c1.2-5.6,2.5-11.3,3.7-16.9

                              c7.5-0.8,14.7-2.7,21.9-4.9c20.9-6.3,41-14.5,61-23.4c6.2-2.8,15.4-5.6,21.4-8.8c-1.4,6.4,1,28.1,1.1,32.3

                              c-19.1,15.4-41.6,30.2-62.6,42.9c-11.6,7-23.4,13.3-36.2,17.9C451.6,538.9,445.2,540.6,438.5,541.3z"/>

                        <path fill="#383838" d="M618.3,198.8c-2.2,1.5-4.7,2.2-7.1,3.2c-10.3,4.4-20.7,8.7-31.1,13.1c-5.7,2.4-11.4,4.7-17.1,7.1

                              c-0.5,0.2-1.1,0.6-1.7,0.3c-13.6-16.4-30.2-29.5-48.1-40.9c-2-1.3-4.8-2.7-7-3.8c4.7-5.5,13.8-31,13.4-36.3c3.1,0.8,13.2,1.8,16.2,3

                              c5.2,2.1,10.4,4.1,15.6,6.4c9.5,4.1,18.5,8.9,27.3,14.3c13.9,8.6,26.7,18.6,37.4,31.1C616.9,197,617.6,197.9,618.3,198.8z"/>

                        <path fill="#383838" d="M185,250.6c3.6,2.8,7.3,5.5,10.9,8.3c7.3,5.7,14.6,11.4,22,17.1c5,3.9,10.1,7.8,15.1,11.7

                              c0.2,0.1,0.2,0.3,0.4,0.5c-4.3,8.7-6.5,18-8.6,27.4c-3.5,15.5-6.8,25.8-7.5,41.7c-5.4-3.7-27-8-35.3-6.8

                              c-4.1-26.4-5.7-53.2-1.7-79.7C181.2,263.9,182.7,257.2,185,250.6z"/>

                        <g id="grp1" class="clk_circle" data-id="1">

                            <g>

                                <path fill="#FFFFFF" d="M380.5,53.8H423c0.9,0,1.6,0.7,1.6,1.6V66c-1,0.9-1.8,2-2.3,3.5l-0.5-0.3c-0.1-0.1-0.3-0.2-0.4-0.3V57

                                      h-39.3v28h39.3v-3l3.2,2.2v2.5c0,0.9-0.7,1.6-1.6,1.6h-42.5c-0.9,0-1.6-0.7-1.6-1.6V55.4C378.9,54.5,379.6,53.8,380.5,53.8z

                                      M419.7,78.4l5.1,3.4l0,0c0.4,0.2,0.8,0.3,1.3,0.3c1.6,0,3-1.3,3-3l0,0v-5.3h1.9v50.6c0,2.2,1.8,4,4,4c2.2,0,4-1.8,4-3.9l0,0V91.9

                                      h1.8v32.6c0,2.2,1.8,4,4,4c2.2,0,4-1.8,4-4V74h1.9v16l0,0v0.1c0,1.6,1.3,3,3,3c1.6,0,3-1.3,3-3V90l0,0V72.7c0,0,0.4-7.4-9.7-7.6

                                      h-3.7l-3.3,5.4l-3.3-5.4h-3.9c0,0-9-0.1-9.6,7.6v1l-3.5-2.4l-0.4-0.3l0,0c-0.6-0.6-1.5-0.9-2.3-0.9c-0.6,0-1.1,0.2-1.6,0.6

                                      L399.6,60l-0.7,1l15.6,10.6c-0.6,1.1-0.5,2.6,0.5,3.6l0,0l0,0l1.9,1.3L419.7,78.4z M446.8,57c0-3.8-3-6.8-6.8-6.8s-6.8,3-6.8,6.8

                                      c0,3.8,3,6.8,6.8,6.8C443.7,63.8,446.8,60.7,446.8,57z"/>

                            </g>

                            <g enable-background="new    ">

                                <path fill="#FFFFFF" d="M378.3,156.8v-15.5h5.2c3.7,0,6.2,1.5,6.2,4.8c0,2.8-1.3,5.1-5.9,5.1c-0.7,0-1.5,0-2.2-0.1v5.7H378.3z

                                      M386.5,146.3L386.5,146.3c0-1.7-1.1-2.7-3-2.7h-2v5.3c0.3,0,1.1,0,1.7,0C386,148.9,386.5,147.6,386.5,146.3z"/>

                                <path fill="#FFFFFF" d="M402.8,151c0,3.5-1.9,6-5.6,6s-5.6-2.5-5.6-6c0-3.5,1.9-6.1,5.6-6.1S402.8,147.5,402.8,151z M399.9,151

                                      c0-2.6-1.1-3.8-2.7-3.8s-2.7,1.3-2.7,3.8c0,2.5,1.1,3.8,2.7,3.8S399.9,153.5,399.9,151z"/>

                                <path fill="#FFFFFF" d="M408.8,152.3c0,1.7,0.2,2.3,1.8,2.3c0.6,0,0.6,0,0.6,0l0.1,2.2c0,0-0.6,0.1-1.6,0.1c-3.5,0-4-2.1-4-5v-11

                                      l3-0.2V152.3z"/>

                                <path fill="#FFFFFF" d="M417,143.4h-3.1v-2.5h3.1V143.4z M416.9,156.8h-3v-11.4h3V156.8z"/>

                                <path fill="#FFFFFF" d="M428.5,154l0.6,2c-1,0.7-1.9,1-3.3,1h0c-3.3,0-5.8-2-5.8-6.2c0-4,2.7-5.9,5.6-5.9h0.1c1.6,0,2.5,0.4,3.4,1

                                      l-0.5,2c-0.5-0.3-1.6-0.8-2.6-0.8c-1.7,0-3.1,1.1-3.1,3.8c0,2.8,1.3,3.9,3.1,3.9C427,154.8,427.9,154.4,428.5,154z"/>

                                <path fill="#FFFFFF" d="M434.7,156.4l-4.6-11h3.1l3,7.6l2.6-7.6h3.1l-4.7,12.2c-1.1,3-2.4,3.6-4.9,3.6h-1.5l-0.3-2.2h0.8

                                      C433.4,159,433.9,159.2,434.7,156.4z"/>

                            </g>

                            <g enable-background="new    ">

                                <path fill="#FFFFFF" d="M366,176.3l2,1.3c-0.5,1-1.1,1.9-1.8,2.7l2.2,2.4l-2.5,0.8l-1.5-1.6c-1.5,1.1-3.1,1.6-4.8,1.6

                                      c-2.7,0-5.6-1.2-5.6-4.6c0-2.4,1.6-3.7,3.4-4.6c-0.8-0.9-1.5-1.9-1.5-3.3c0-2.6,2-3.6,4-3.6c2,0,4.2,1,4.2,3.4c0,2.1-1.7,3.2-3.3,4

                                      l3.6,3.8C365.1,177.9,365.5,177.1,366,176.3z M356.7,178.6c0,1.9,1.4,2.6,3,2.6c1.1,0,2.1-0.4,3-1l-3.9-4.6

                                      C357.6,176.2,356.7,177,356.7,178.6z M361.5,170.9c0-1-0.6-1.5-1.5-1.5c-0.9,0-1.6,0.5-1.6,1.6c0,1,0.5,1.7,1.1,2.4

                                      C360.4,172.8,361.5,172.2,361.5,170.9z"/>

                                <path fill="#FFFFFF" d="M382.8,183.2h-3.2V170h-4.5v-2.4h12.3v2.4h-4.6V183.2z"/>

                                <path fill="#FFFFFF" d="M395.4,171.8l0.2,2.3h-0.8c-2.2,0-2.6,1.1-2.6,2.8v6.3h-3v-11.4h2.8l0.2,1.6c0.7-1,1.5-1.6,2.9-1.6H395.4z"

                                      />

                                <path fill="#FFFFFF" d="M400.5,183.4c-2.1,0-3.6-1.3-3.6-3.3c0-2.9,2-3.4,5.8-3.9l0.8-0.1v-0.5c0-1.3-0.1-1.9-1.7-2h-0.2

                                      c-1.1,0-3,0.6-3.5,0.8l-0.6-2.2c1.1-0.5,2.5-0.8,4.4-0.9h0.3c2.6,0,4.4,1.2,4.4,3.7v8.1h-2.3L404,182

                                      C403,182.8,402,183.4,400.5,183.4z M403.6,180.5v-2.7l-0.8,0.1c-1.7,0.2-3.1,0.5-3.1,2.1c0,0.8,0.5,1.4,1.4,1.4

                                      C402.1,181.5,402.7,181.1,403.6,180.5z"/>

                                <path fill="#FFFFFF" d="M413.1,169.8H410v-2.5h3.1V169.8z M413.1,183.2h-3v-11.4h3V183.2z"/>

                                <path fill="#FFFFFF" d="M422.2,173.6c-1.1,0-2.3,0.9-2.8,1.8v7.8h-3.1v-11.4h2.6l0.2,1.5c0.9-1.1,2.3-2,4-2c2.8,0,4,2.1,4,4.9v7

                                      h-3.1v-6.7C424.1,174.9,424.1,173.6,422.2,173.6z"/>

                                <path fill="#FFFFFF" d="M433.6,169.8h-3.1v-2.5h3.1V169.8z M433.6,183.2h-3v-11.4h3V183.2z"/>

                                <path fill="#FFFFFF" d="M442.7,173.6c-1.1,0-2.3,0.9-2.8,1.8v7.8h-3.1v-11.4h2.6l0.2,1.5c0.9-1.1,2.3-2,4-2c2.8,0,4,2.1,4,4.9v7

                                      h-3.1v-6.7C444.6,174.9,444.5,173.6,442.7,173.6z"/>

                                <path fill="#FFFFFF" d="M453.5,188.4l-0.2-2.4h1.2c3.3,0,4.5,0,4.5-3.5v-0.9c-0.6,0.8-1.4,1.5-3.3,1.5c-2.9,0-5.3-2.2-5.3-5.9

                                      c0-4,2.6-6,5.1-6h0c1.8,0,2.8,0.7,3.4,1.6v-1.1h3v10.7c0,4.1-1.6,5.9-7,5.9H453.5z M456.3,181c1.6,0,2.8-1.4,2.8-3.7

                                      c0-2.3-1.2-3.7-2.8-3.7h-0.1c-1.9,0-2.9,1.5-2.9,3.7C453.3,179.5,454.5,181,456.3,181L456.3,181z"/>

                            </g>

                            <circle id="teaddd" class="" fill="transparent" stroke="#FFFFFF" stroke-width="4" stroke-miterlimit="10" cx="414.6" cy="123" r="106.7"/>

                        </g>

                        <g id="grp2" class="clk_circle" data-id="2">

                            <g>

                                <path fill="#FFFFFF" d="M150.3,450.4v-46h17v-17h32.2v43.9c0.5,0,1.1-0.1,1.6-0.1c1,0,1.9,0.1,2.9,0.2v-48.6h-39.6l-18.6,19.7v52.3

                                      h28.8c-0.1-1.4-0.2-2.8-0.4-4.5L150.3,450.4z M162.8,391.2v8.7h-8.3L162.8,391.2z"/>

                                <rect x="155.6" y="417.1" fill="#FFFFFF" width="38.1" height="5.1"/>

                                <path fill="#FFFFFF" d="M186.8,427.1h-31.2v5.1l31.3,0.2c0-2.6,0.1-3.3,0.2-5.3L186.8,427.1L186.8,427.1z"/>

                                <path fill="#FFFFFF" d="M155.6,442.6h25.9c0-1.8,0.1-2.4,0-5.1h-25.9V442.6z"/>

                                <polygon fill="#FFFFFF" points="199.7,434.2 204,434.2 203.6,454.9 199.7,454.9 	"/>

                                <polygon fill="#FFFFFF" points="206.5,422 210.8,422 210.8,454.9 206.5,454.9 	"/>

                                <polygon fill="#FFFFFF" points="192.5,427.3 196.2,427.3 196.2,455.2 192.5,455.3 	"/>

                                <rect x="185.5" y="437.5" fill="#FFFFFF" width="3.1" height="17.3"/>

                                <rect x="178.7" y="446" fill="#FFFFFF" width="3.1" height="8.9"/>

                            </g>

                            <g enable-background="new    ">

                                <path fill="#FFFFFF" d="M151.6,473.3l-4,8h-2.3l-4.1-8.1v10.3h-2.8V468h3l5.1,9.8l5-9.8h3v15.5h-2.9V473.3z"/>

                                <path fill="#FFFFFF" d="M160.4,478.4c0.1,2.1,1.2,3.1,3.2,3.1c1.4,0,2.6-0.5,3.3-1l0.8,1.9c-1,0.8-2.3,1.3-4.2,1.3

                                      c-4,0-6.1-2.4-6.1-6c0-3.4,2-6.1,5.6-6.1c3.5,0,5.1,2.8,5.1,5.8v0c0,0.3,0,0.6,0,0.9v0H160.4z M165.3,476.8v-0.1

                                      c0-1.8-0.8-2.8-2.3-2.8c-1,0-2.5,0.6-2.6,2.9H165.3z"/>

                                <path fill="#FFFFFF" d="M174.3,478.8c0,1.9,0.1,2.6,1.8,2.6c0.1,0,1-0.1,1-0.1l0.2,2.2c0,0-1.1,0.1-1.7,0.1c-3.5,0-4.2-1.7-4.2-5

                                      v-4.7h-2l0.2-1.8h1.8l0.7-4.2h2.3v4.2h2.8v1.8h-2.8V478.8z"/>

                                <path fill="#FFFFFF" d="M186.3,472.1l0.2,2.3h-0.8c-2.2,0-2.6,1.1-2.6,2.8v6.3h-3v-11.4h2.8l0.2,1.6c0.7-1,1.5-1.6,2.9-1.6H186.3z"

                                      />

                                <path fill="#FFFFFF" d="M191.5,470.1h-3.1v-2.5h3.1V470.1z M191.5,483.5h-3v-11.4h3V483.5z"/>

                                <path fill="#FFFFFF" d="M203,480.7l0.6,2c-1,0.7-1.9,1-3.3,1h0c-3.3,0-5.8-2-5.8-6.2c0-4,2.7-5.9,5.6-5.9h0.1c1.6,0,2.5,0.4,3.4,1

                                      l-0.5,2c-0.5-0.3-1.6-0.8-2.6-0.8c-1.7,0-3.1,1.1-3.1,3.8c0,2.8,1.3,3.9,3.1,3.9C201.5,481.5,202.4,481.1,203,480.7z"/>

                                <path fill="#FFFFFF" d="M213.1,474.6c-0.8-0.4-2.2-0.7-3.1-0.7c-1.2,0-1.7,0.3-1.8,0.9c0,0.8,0.3,1,1.3,1.3l1.8,0.6

                                      c1.4,0.5,2.8,1.5,2.8,3.3c0,1.9-1.3,3.7-3.9,3.7c-2.1,0-3.7-0.4-4.8-1.1l0.6-2c0.9,0.6,2.7,0.9,3.8,0.9c0.9,0,1.5-0.3,1.5-1.2

                                      c0-0.7-0.2-0.9-1.1-1.2l-1.7-0.6c-1.7-0.6-3-1.5-3-3.3c0-2.4,1.8-3.5,4.2-3.5c1.6,0,2.9,0.3,3.9,0.9L213.1,474.6z"/>

                            </g>

                            <g enable-background="new    ">

                                <path fill="#FFFFFF" d="M126.9,503l2,1.3c-0.5,1-1.1,1.9-1.8,2.7l2.2,2.4l-2.5,0.8l-1.5-1.6c-1.5,1.1-3.1,1.6-4.8,1.6

                                      c-2.7,0-5.6-1.2-5.6-4.6c0-2.4,1.6-3.7,3.4-4.6c-0.8-0.9-1.5-1.9-1.5-3.3c0-2.6,2-3.6,4-3.6c2,0,4.2,1,4.2,3.4c0,2.1-1.7,3.2-3.3,4

                                      l3.6,3.8C126,504.6,126.5,503.8,126.9,503z M117.6,505.3c0,1.9,1.4,2.6,3,2.6c1.1,0,2.1-0.4,3-1l-3.9-4.6

                                      C118.6,502.9,117.6,503.7,117.6,505.3z M122.4,497.6c0-1-0.6-1.5-1.5-1.5c-0.9,0-1.6,0.5-1.6,1.6c0,1,0.6,1.7,1.1,2.4

                                      C121.3,499.5,122.4,498.9,122.4,497.6z"/>

                                <path fill="#FFFFFF" d="M145.6,505.1H141l-1.7,4.8h-3.2l0.4-1.3l5.2-14.2h3l5.1,14.2l0.4,1.3h-3L145.6,505.1z M144.9,503.2

                                      l-1.5-5.3l-1.6,5.3H144.9z"/>

                                <path fill="#FFFFFF" d="M158.2,500.3c-1.1,0-2.3,0.9-2.8,1.8v7.8h-3.1v-11.4h2.6l0.2,1.5c0.9-1.1,2.3-2,4-2c2.8,0,4,2.1,4,4.9v7

                                      H160v-6.7C160,501.7,160,500.3,158.2,500.3z"/>

                                <path fill="#FFFFFF" d="M169.4,510.1c-2.1,0-3.6-1.3-3.6-3.3c0-2.9,2-3.4,5.8-3.9l0.8-0.1v-0.5c0-1.3-0.1-1.9-1.7-2h-0.2

                                      c-1.1,0-3,0.6-3.5,0.8l-0.6-2.2c1.1-0.5,2.5-0.8,4.4-0.9h0.3c2.6,0,4.4,1.2,4.4,3.7v8.1h-2.3l-0.3-1.2

                                      C171.8,509.5,170.9,510.1,169.4,510.1z M172.4,507.2v-2.7l-0.8,0.1c-1.7,0.2-3.1,0.5-3.1,2.1c0,0.8,0.5,1.4,1.4,1.4

                                      C171,508.2,171.6,507.8,172.4,507.2z"/>

                                <path fill="#FFFFFF" d="M181.8,505.4c0,1.7,0.2,2.3,1.8,2.3c0.6,0,0.6,0,0.6,0l0.1,2.2c0,0-0.6,0.1-1.6,0.1c-3.5,0-4-2.1-4-5v-11

                                      l3-0.2V505.4z"/>

                                <path fill="#FFFFFF" d="M190,509.5l-4.6-11h3.1l3,7.6l2.6-7.6h3.1l-4.7,12.2c-1.1,3-2.4,3.6-4.9,3.6h-1.5l-0.3-2.2h0.8

                                      C188.7,512.1,189.2,512.3,190,509.5z"/>

                                <path fill="#FFFFFF" d="M202.8,505.2c0,1.9,0.1,2.6,1.8,2.6c0.1,0,1-0.1,1-0.1l0.2,2.2c0,0-1.1,0.1-1.7,0.1c-3.5,0-4.2-1.7-4.2-5

                                      v-4.7h-2l0.2-1.8h1.8l0.7-4.2h2.3v4.2h2.8v1.8h-2.8V505.2z"/>

                                <path fill="#FFFFFF" d="M211.8,496.5h-3.1V494h3.1V496.5z M211.8,509.9h-3v-11.4h3V509.9z"/>

                                <path fill="#FFFFFF" d="M223.4,507.1l0.6,2c-1,0.7-1.9,1-3.3,1h0c-3.3,0-5.8-2-5.8-6.2c0-4,2.7-5.9,5.6-5.9h0.1

                                      c1.6,0,2.5,0.4,3.4,1l-0.5,2c-0.5-0.3-1.6-0.8-2.6-0.8c-1.7,0-3.1,1.1-3.1,3.8c0,2.8,1.3,3.9,3.1,3.9

                                      C221.8,507.9,222.7,507.5,223.4,507.1z"/>

                                <path fill="#FFFFFF" d="M233.5,501c-0.8-0.4-2.2-0.7-3.1-0.7c-1.2,0-1.7,0.3-1.8,0.9c0,0.8,0.3,1,1.3,1.3l1.8,0.6

                                      c1.4,0.5,2.8,1.5,2.8,3.3c0,1.9-1.3,3.7-3.9,3.7c-2.1,0-3.7-0.4-4.8-1.1l0.6-2c0.9,0.6,2.7,0.9,3.8,0.9c0.9,0,1.5-0.3,1.5-1.2

                                      c0-0.7-0.2-0.9-1.1-1.2l-1.7-0.6c-1.7-0.6-3-1.5-3-3.3c0-2.4,1.8-3.5,4.2-3.5c1.6,0,2.9,0.3,3.9,0.9L233.5,501z"/>

                            </g>

                            <circle fill="transparent" stroke="#FFFFFF" stroke-width="4" stroke-miterlimit="10" cx="179.1" cy="457.2" r="106.7"/>

                        </g>



                        <g id="grp3" class="clk_circle" data-id="3">

                            <g>

                                <path fill="#FFFFFF" d="M641,414.3c0-0.4-0.1-0.7-0.3-1L630,401.2c-0.2-0.3-0.6-0.4-0.9-0.5c-0.4,0-0.7,0.1-1,0.3

                                      c-4.9,4.3-9,9.4-12.1,15.1c-0.2,0.3-0.2,0.7-0.1,1c0.1,0.3,0.3,0.6,0.6,0.8l14.2,7.7c0.2,0.1,0.4,0.2,0.6,0.2

                                      c0.5,0,0.9-0.3,1.2-0.7c2.1-3.8,4.8-7.1,8-10C640.8,414.9,640.9,414.6,641,414.3z"/>

                                <path fill="#FFFFFF" d="M645.7,409.8c0.2,0.5,0.7,0.7,1.2,0.7c0.2,0,0.4-0.1,0.6-0.2c3.8-2,7.9-3.3,12.1-3.9

                                      c0.7-0.1,1.3-0.8,1.2-1.5l-2.2-16c-0.1-0.7-0.8-1.3-1.5-1.2c-6.5,0.9-12.7,2.9-18.4,5.9c-0.7,0.3-0.9,1.2-0.6,1.8L645.7,409.8z"/>

                                <path fill="#FFFFFF" d="M628.4,431.4l-15.6-4.2c-0.3-0.1-0.7,0-1,0.1s-0.5,0.5-0.6,0.8c-1.3,4.7-1.9,9.6-1.9,14.5

                                      c0,0.8,0,1.7,0.1,2.6c0,0.7,0.6,1.3,1.3,1.3c0,0,0,0,0.1,0l16.1-0.7c0.4,0,0.7-0.2,0.9-0.4c0.2-0.3,0.4-0.6,0.3-1

                                      c0-0.6,0-1.2,0-1.7c0-3.3,0.4-6.5,1.3-9.6C629.5,432.4,629.1,431.6,628.4,431.4z"/>

                                <path fill="#FFFFFF" d="M688.7,392.7c-5.9-2.8-12.1-4.6-18.6-5.2c-0.4,0-0.7,0.1-1,0.3c-0.3,0.2-0.4,0.6-0.5,0.9l-1.6,16.1

                                      c-0.1,0.7,0.5,1.4,1.2,1.5c4.3,0.4,8.4,1.6,12.3,3.4c0.2,0.1,0.4,0.1,0.6,0.1c0.1,0,0.3,0,0.4-0.1c0.3-0.1,0.6-0.4,0.8-0.7l7-14.5

                                      C689.7,393.8,689.4,393,688.7,392.7z"/>

                                <path fill="#FFFFFF" d="M712.4,415.3c0.1-0.3,0-0.7-0.2-1c-3.3-5.6-7.6-10.5-12.7-14.6c-0.3-0.2-0.6-0.3-1-0.3

                                      c-0.4,0-0.7,0.2-0.9,0.5l-10.2,12.5c-0.2,0.3-0.3,0.6-0.3,1c0,0.4,0.2,0.7,0.5,0.9c3.3,2.7,6.2,6,8.4,9.7c0.3,0.4,0.7,0.7,1.2,0.7

                                      c0.2,0,0.5-0.1,0.7-0.2l13.9-8.3C712.1,415.9,712.3,415.6,712.4,415.3z"/>

                                <path fill="#FFFFFF" d="M717.6,426.1c-0.2-0.7-1-1.1-1.7-0.9l-15.4,4.8c-0.7,0.2-1.1,1-0.9,1.7c1.1,3.5,1.7,7.2,1.7,11

                                      c0,0.6,0,1.1,0,1.7c0,0.4,0.1,0.7,0.3,1s0.6,0.4,0.9,0.4l16.1,0.7c0,0,0,0,0.1,0c0.7,0,1.3-0.6,1.3-1.3c0-0.9,0.1-1.7,0.1-2.6

                                      C720.1,437,719.3,431.4,717.6,426.1z"/>

                                <path fill="#FFFFFF" d="M656.6,435.8c-1.8,2.9-2.3,6.2-1.6,9.5c0.7,3.3,2.8,6.1,5.6,7.9c2.9,1.8,6.2,2.3,9.5,1.6

                                      c3.3-0.8,6.1-2.8,7.9-5.6c1.8-2.9,2.3-6.2,1.6-9.5c-0.1-0.4-0.2-0.8-0.4-1.3l6.5-24.5c0.2-0.6-0.1-1.2-0.6-1.5

                                      c-0.5-0.3-1.2-0.2-1.7,0.2L665.1,430c-0.2,0-0.5,0.1-0.7,0.1C661.1,430.9,658.3,432.9,656.6,435.8z"/>

                            </g>

                            <g enable-background="new    ">

                                <path fill="#FFFFFF" d="M602.2,472.3l-4,8h-2.3l-4.1-8.1v10.3H589V467h3l5.1,9.8l5-9.8h3v15.5h-2.9V472.3z"/>

                                <path fill="#FFFFFF" d="M611.6,482.7c-2.1,0-3.6-1.3-3.6-3.3c0-2.9,2-3.4,5.8-3.9l0.8-0.1v-0.5c0-1.3-0.1-1.9-1.7-2h-0.2

                                      c-1.1,0-3,0.6-3.5,0.8l-0.6-2.2c1.1-0.5,2.5-0.8,4.4-0.9h0.3c2.6,0,4.4,1.2,4.4,3.7v8.1h-2.3l-0.3-1.2

                                      C614.1,482.1,613.1,482.7,611.6,482.7z M614.6,479.8v-2.7l-0.8,0.1c-1.7,0.2-3.1,0.5-3.1,2.1c0,0.8,0.5,1.4,1.4,1.4

                                      C613.2,480.8,613.8,480.4,614.6,479.8z"/>

                                <path fill="#FFFFFF" d="M626.7,472.9c-1.1,0-2.3,0.9-2.8,1.8v7.8h-3.1v-11.4h2.6l0.2,1.5c0.9-1.1,2.3-2,4-2c2.8,0,4,2.1,4,4.9v7

                                      h-3.1v-6.7C628.6,474.3,628.5,472.9,626.7,472.9z"/>

                                <path fill="#FFFFFF" d="M637.9,482.7c-2.1,0-3.6-1.3-3.6-3.3c0-2.9,2-3.4,5.8-3.9l0.8-0.1v-0.5c0-1.3-0.1-1.9-1.7-2h-0.2

                                      c-1.1,0-3,0.6-3.5,0.8l-0.6-2.2c1.1-0.5,2.5-0.8,4.4-0.9h0.3c2.6,0,4.4,1.2,4.4,3.7v8.1h-2.3l-0.3-1.2

                                      C640.4,482.1,639.4,482.7,637.9,482.7z M640.9,479.8v-2.7l-0.8,0.1c-1.7,0.2-3.1,0.5-3.1,2.1c0,0.8,0.5,1.4,1.4,1.4

                                      C639.5,480.8,640.1,480.4,640.9,479.8z"/>

                                <path fill="#FFFFFF" d="M649.9,487.7l-0.2-2.4h1.2c3.3,0,4.5,0,4.5-3.5V481c-0.6,0.8-1.4,1.5-3.3,1.5c-2.9,0-5.3-2.2-5.3-5.9

                                      c0-4,2.6-6,5.1-6h0c1.8,0,2.8,0.7,3.4,1.6v-1.1h3v10.7c0,4.1-1.6,5.9-7,5.9H649.9z M652.7,480.3c1.6,0,2.8-1.4,2.8-3.7

                                      c0-2.3-1.2-3.7-2.8-3.7h-0.1c-1.9,0-2.9,1.5-2.9,3.7C649.7,478.8,650.9,480.3,652.7,480.3L652.7,480.3z"/>

                                <path fill="#FFFFFF" d="M664.2,477.4c0.1,2.1,1.2,3.1,3.2,3.1c1.4,0,2.6-0.5,3.3-1l0.8,1.9c-1,0.8-2.3,1.3-4.2,1.3

                                      c-4,0-6.1-2.4-6.1-6c0-3.4,2-6.1,5.6-6.1c3.5,0,5.1,2.8,5.1,5.8v0c0,0.3,0,0.6,0,0.9v0H664.2z M669.1,475.8v-0.1

                                      c0-1.8-0.8-2.8-2.3-2.8c-1,0-2.5,0.6-2.6,2.9H669.1z"/>

                                <path fill="#FFFFFF" d="M677.5,482.5h-3v-11.4h2.6l0.2,1.5c0.8-1,2.2-1.9,3.9-2h0.1c1.5,0,2.5,0.8,2.9,1.9c1-1.2,2.3-1.9,4-1.9h0.1

                                      c2.8,0,3.8,2.1,3.8,4.9v7h-3v-6.4c0-2.2-0.2-3.3-1.5-3.3h-0.1c-1.4,0-2.2,0.9-2.6,1.8v7.8h-3v-6.6c0-1.7-0.2-3.1-1.7-3.1

                                      c-1.3,0-2.1,1-2.6,1.8V482.5z"/>

                                <path fill="#FFFFFF" d="M697.7,477.4c0.1,2.1,1.2,3.1,3.2,3.1c1.4,0,2.6-0.5,3.3-1l0.8,1.9c-1,0.8-2.3,1.3-4.2,1.3

                                      c-4,0-6.1-2.4-6.1-6c0-3.4,2-6.1,5.6-6.1c3.5,0,5.1,2.8,5.1,5.8v0c0,0.3,0,0.6,0,0.9v0H697.7z M702.7,475.8v-0.1

                                      c0-1.8-0.8-2.8-2.3-2.8c-1,0-2.5,0.6-2.6,2.9H702.7z"/>

                                <path fill="#FFFFFF" d="M713.9,472.9c-1.1,0-2.3,0.9-2.8,1.8v7.8H708v-11.4h2.6l0.2,1.5c0.9-1.1,2.3-2,4-2c2.8,0,4,2.1,4,4.9v7

                                      h-3.1v-6.7C715.8,474.3,715.7,472.9,713.9,472.9z"/>

                                <path fill="#FFFFFF" d="M725.5,477.8c0,1.9,0.1,2.6,1.8,2.6c0.1,0,1-0.1,1-0.1l0.2,2.2c0,0-1.1,0.1-1.7,0.1c-3.5,0-4.2-1.7-4.2-5

                                      v-4.7h-2l0.2-1.8h1.8l0.7-4.2h2.3v4.2h2.8v1.8h-2.8V477.8z"/>

                            </g>

                            <g enable-background="new    ">

                                <path fill="#FFFFFF" d="M595.6,502l2,1.3c-0.5,1-1.1,1.9-1.8,2.7l2.2,2.4l-2.5,0.8l-1.5-1.6c-1.5,1.1-3.1,1.6-4.8,1.6

                                      c-2.7,0-5.6-1.2-5.6-4.6c0-2.4,1.6-3.7,3.4-4.6c-0.8-0.9-1.5-1.9-1.5-3.3c0-2.6,2-3.6,4-3.6c2,0,4.2,1,4.2,3.4c0,2.1-1.7,3.2-3.3,4

                                      l3.6,3.8C594.7,503.6,595.2,502.8,595.6,502z M586.4,504.3c0,1.9,1.4,2.6,3,2.6c1.1,0,2.1-0.4,3-1l-3.9-4.6

                                      C587.3,501.9,586.4,502.7,586.4,504.3z M591.2,496.6c0-1-0.6-1.5-1.5-1.5c-0.9,0-1.6,0.5-1.6,1.6c0,1,0.5,1.7,1.1,2.4

                                      C590.1,498.5,591.2,497.9,591.2,496.6z"/>

                                <path fill="#FFFFFF" d="M613.5,509.1c-4.2,0-7.7-2.9-7.7-8.1c0-4.9,3.4-8,7.7-8c1.6,0,3.3,0.3,4.7,1.5l-0.8,2.3

                                      c-0.9-0.7-1.8-1.3-3.7-1.3c-2.7,0-4.7,2.1-4.7,5.5c0,3.7,1.9,5.6,4.6,5.6c1.4,0,2.7-0.4,3.8-1.2l0.8,2.3

                                      C616.7,508.9,615.1,509.1,613.5,509.1z"/>

                                <path fill="#FFFFFF" d="M631.7,503.1c0,3.5-1.9,6-5.6,6c-3.7,0-5.6-2.5-5.6-6c0-3.5,1.9-6.1,5.6-6.1

                                      C629.8,497,631.7,499.6,631.7,503.1z M628.8,503.1c0-2.6-1.1-3.8-2.7-3.8c-1.6,0-2.7,1.3-2.7,3.8c0,2.5,1.1,3.8,2.7,3.8

                                      C627.7,506.9,628.8,505.6,628.8,503.1z"/>

                                <path fill="#FFFFFF" d="M637.5,508.9h-3v-11.4h2.6l0.2,1.5c0.8-1,2.2-1.9,3.9-2h0.1c1.5,0,2.5,0.8,2.9,1.9c1-1.2,2.3-1.9,4-1.9h0.1

                                      c2.8,0,3.8,2.1,3.8,4.9v7h-3v-6.4c0-2.2-0.2-3.3-1.5-3.3h-0.1c-1.4,0-2.2,0.9-2.6,1.8v7.8h-3v-6.6c0-1.7-0.2-3.1-1.7-3.1

                                      c-1.3,0-2.1,1-2.6,1.8V508.9z"/>

                                <path fill="#FFFFFF" d="M658.1,499c0.7-1.5,1.9-2,3.4-2c3.5,0,5.2,3,5.2,6c0,3.3-1.8,6.1-5.1,6.1h-0.1c-1.6,0-2.7-0.5-3.4-1.6v5.5

                                      l-3,0.3v-15.8h2.8L658.1,499z M663.8,503.1c0-3.1-1.5-3.8-2.8-3.8c-1.4,0-2.8,0.9-2.8,3.8c0,2.9,1.4,3.8,2.9,3.8

                                      C662.4,506.9,663.8,506,663.8,503.1z"/>

                                <path fill="#FFFFFF" d="M672.7,504.4c0,1.7,0.2,2.3,1.8,2.3c0.5,0,0.6,0,0.6,0l0.1,2.2c0,0-0.6,0.1-1.6,0.1c-3.5,0-4-2.1-4-5v-11

                                      l3-0.2V504.4z"/>

                                <path fill="#FFFFFF" d="M680.9,495.5h-3.1V493h3.1V495.5z M680.8,508.9h-3v-11.4h3V508.9z"/>

                                <path fill="#FFFFFF" d="M687.4,509.1c-2.1,0-3.6-1.3-3.6-3.3c0-2.9,2-3.4,5.8-3.9l0.8-0.1v-0.5c0-1.3-0.1-1.9-1.7-2h-0.2

                                      c-1.1,0-3,0.6-3.5,0.8l-0.6-2.2c1.1-0.5,2.5-0.8,4.4-0.9h0.3c2.6,0,4.4,1.2,4.4,3.7v8.1h-2.3l-0.3-1.2

                                      C689.8,508.5,688.9,509.1,687.4,509.1z M690.4,506.2v-2.7l-0.8,0.1c-1.7,0.2-3.1,0.5-3.1,2.1c0,0.8,0.5,1.4,1.4,1.4

                                      C689,507.2,689.6,506.8,690.4,506.2z"/>

                                <path fill="#FFFFFF" d="M702.5,499.3c-1.1,0-2.3,0.9-2.8,1.8v7.8h-3.1v-11.4h2.6l0.2,1.5c0.9-1.1,2.3-2,4-2c2.8,0,4,2.1,4,4.9v7

                                      h-3.1v-6.7C704.4,500.7,704.3,499.3,702.5,499.3z"/>

                                <path fill="#FFFFFF" d="M718.7,506.1l0.6,2c-1,0.7-1.9,1-3.3,1h0c-3.3,0-5.8-2-5.8-6.2c0-4,2.7-5.9,5.6-5.9h0.1

                                      c1.6,0,2.5,0.4,3.4,1l-0.5,2c-0.5-0.3-1.6-0.8-2.6-0.8c-1.7,0-3.1,1.1-3.1,3.8c0,2.8,1.3,3.9,3.1,3.9

                                      C717.2,506.9,718.1,506.5,718.7,506.1z"/>

                                <path fill="#FFFFFF" d="M724.2,503.8c0.1,2.1,1.2,3.1,3.2,3.1c1.4,0,2.6-0.5,3.3-1l0.8,1.9c-1,0.8-2.3,1.3-4.2,1.3

                                      c-4,0-6.1-2.4-6.1-6c0-3.4,2-6.1,5.6-6.1c3.5,0,5.1,2.8,5.1,5.8v0c0,0.3,0,0.6,0,0.9v0H724.2z M729.1,502.2v-0.1

                                      c0-1.8-0.8-2.8-2.3-2.8c-1,0-2.5,0.6-2.6,2.9H729.1z"/>

                            </g>

                            <circle fill="transparent" stroke="#FFFFFF" stroke-width="4" stroke-miterlimit="10" cx="662.2" cy="456.5" r="106.7"/>

                        </g>

                    </svg>







                </div>

                <div class="smart_sns_box2" style="background-color:rgba(147, 185, 36,1);">



                    <div id="sns1" class="box_contant">

                        <h3>Driving increasing results is a true partnership</h3>

                        <p>Indyme’s reports provide unique insights that drive policies. With corporate support and training, these policies can be tested and measured objectively.</p>

                        <div class="indybtn">

                            <a href="<?php bloginfo('url'); ?>/solutions" class="btn btn-success transparent">LEARN MORE</a>

                        </div>

                    </div>

                    <div id="sns2" class="box_contant">

                        <h3>Keep Driving Results</h3>

                        <p>Indyme’s secure, cloud-based reporting system does not just provide data, it gives you results-focused, actionable insight.</p>

                        <div class="indybtn">

                            <a href="<?php bloginfo('url'); ?>/solutions" class="btn btn-success transparent">LEARN MORE</a>

                        </div>

                    </div>

                    <div id="sns3" class="box_contant">

                        <h3>Performance you can measure</h3>

                        <p>The compliance and success data enables management to create a competitive spirit among stores and drive performance. </p>

                        <div class="indybtn">

                            <a href="<?php bloginfo('url'); ?>/solutions" class="btn btn-success transparent">LEARN MORE</a>

                        </div>

                    </div>

                </div>

            </div>

            <div class="hiw_wrapper">

                <div class="tbspace container">

                    <h2 class="page_heading">A trial process designed for you</h2>

                    <div class="row d_team">

                        <div class="col-lg-3 col-sm-3">

                            <div class="hiwToggle text-center" data-id="1">

                                <div class="hiw_txt"><p><span>1</span><?php echo get_field('trial_process_title1'); ?></p><i class="arr_up_dwn fa fa-angle-down"></i></div>

                            </div>

                        </div>

                        <div class="col-lg-3 col-sm-3">

                            <div class="hiwToggle text-center" data-id="2">

                                <div class="hiw_txt"><p><span>2</span><?php echo get_field('trial_process_title2'); ?></p><i class="arr_up_dwn fa fa-angle-down"></i></div>

                            </div>

                        </div>

                        <div class="col-lg-3 col-sm-3">

                            <div class="hiwToggle text-center" data-id="3">

                                <div class="hiw_txt"><p><span>3</span><?php echo get_field('trial_process_title3'); ?></p><i class="arr_up_dwn fa fa-angle-down"></i></div>

                            </div>

                        </div>

                        <div class="col-lg-3 col-sm-3">

                            <div class="hiwToggle text-center" data-id="4">

                                <div class="hiw_txt"><p><span>4</span><?php echo get_field('trial_process_title4'); ?></p><i class="arr_up_dwn fa fa-angle-down"></i></div>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="hiwToggle text-center m_team" data-id="1">

                            <div class="hiw_txt"><p><span>1</span>Identify Key Metrics</p><i class="arr_up_dwn fa fa-angle-down"></i></div>

                        </div>

                        <div class="hiwTogglebox">

                            <?php echo get_field('trial_process_content1'); ?>

                        </div>

                        <div class="hiwToggle text-center m_team" data-id="2">

                            <div class="hiw_txt"><p><span>2</span>Tailor fit solutions</p><i class="arr_up_dwn fa fa-angle-down"></i></div>

                        </div>

                        <div class="hiwTogglebox">

                            <?php echo get_field('trial_process_content2'); ?>

                        </div>

                        <div class="hiwToggle text-center m_team" data-id="3">

                            <div class="hiw_txt"><p><span>3</span>Install and optimize</p><i class="arr_up_dwn fa fa-angle-down"></i></div>

                        </div>

                        <div class="hiwTogglebox">

                            <?php echo get_field('trial_process_content3'); ?>

                        </div>

                        <div class="hiwToggle text-center m_team" data-id="4">

                            <div class="hiw_txt"><p><span>4</span>Measure results</p><i class="arr_up_dwn fa fa-angle-down"></i></div>

                        </div>

                        <div class="hiwTogglebox">

                            <?php echo get_field('trial_process_content4'); ?>

                        </div>

                    </div> 

                </div>

            </div>



        </div>

        <div id="request" class="section_4 ">

            <div class="container">

                <h2>WE'RE READY TO HELP YOU: <span style="color:#a8c716;">REQUEST A DEMO</span></h2>

                <br />

                <div class="home_form">

                    <?php echo do_shortcode('[contact-form-7 id="57" title="Home Contact us"]'); ?>

                </div>

            </div>

        </div>



        <?php get_footer(); ?>

        <script src="<?php echo bloginfo('template_url') ?>/js/feature.presenter.1.6.min.js"></script>



        <script>



            /* Image path can be relative or absolute. Heading & Description can contain HTML */

            var settings = [{image: '<?php echo bloginfo('template_url') ?>/images/1.png', heading: 'WE KEEP DRIVING RESULT', description: 'Getting continuous results is a true partnership.The data provides unique insights that guide policies. corporte support and training are critical in driving in compliance and success, measured objectively by our metrics'},

                {image: '<?php echo bloginfo('template_url') ?>/images/3.png', heading: 'WE KEEP DRIVING RESULT', description: 'Getting continuous results is a true partnership.The data provides unique insights that guide policies. corporte support and training are critical in driving in compliance and success, measured objectively by our metrics'},

                {image: '<?php echo bloginfo('template_url') ?>/images/2.png', heading: 'WE KEEP DRIVING RESULT', description: 'Getting continuous results is a true partnership.The data provides unique insights that guide policies. corporte support and training are critical in driving in compliance and success, measured objectively by our metrics'}



            ];



            var options = {

                circle_radius: 220,

                normal_feature_size: 100,

                highlighted_feature_size: 150,

                top_margin: 100,

                bottom_margin: 50,

                spacing: 40,

                min_padding: 50,

                heading_font_size: 30,

                description_font_size: 20,

                animation_mode: 'css',

                type: 'image'

            };



            var fp = new FeaturePresenter($("#test-element"), settings, options);





        </script>

        <script>

            $(document).ready(function () {

                $('#g1').tooltip({title: 'Any customer request', container: 'body'});

                $('#g2').tooltip({title: 'Request for cashier assistance or any other director assistance requests.', container: 'body'});

                $('#g3').tooltip({title: 'Video triggered events', container: 'body'});

                $('#g4').tooltip({title: 'Prolonged customer presence and merchandise movements.', container: 'body'});

                $('#g5').tooltip({title: 'Any in store event that needs to be monitored.', container: 'body'});

                $('#g6').tooltip({title: 'All events are accessible through Indyme\'s secure, cloud-based platform and is translated into useful reports and analytics.', container: 'body'});

                $('#g7').tooltip({title: 'Smart Response integrates with any communication devices currently used in-store.Notifications can be sent through walkies.', container: 'body'});

                $('#g8').tooltip({title: 'Send notifications to PDA and mobile devices.', container: 'body'});

                $('#g9').tooltip({title: 'Broadcast notifications through PA systems.', container: 'body'});

                $('#g10').tooltip({title: 'Smart PVM switches displays upon sensing suspicious activity.', container: 'body'});

                $('#g11').tooltip({title: 'Notifications can be sent through walkies.', container: 'body'});

                $('#mid-circle').tooltip({title: 'Smart Response easily integrates into your system to detect desired events and send notifications through existing devices.', container: 'body'})



                load_script(1);

                $(".clk_circle").click(function () {

                    $(".clk_circle").attr("class", "clk_circle");

                    $(".clk_circle").find("circle").attr("stroke", '#FFFFFF');

                    $(".clk_circle").find("path").attr("fill", '#FFFFFF');

                    $(".clk_circle").find("rect").attr("fill", '#FFFFFF');

                    $(".clk_circle").find("polygon").attr("fill", '#FFFFFF');



                    load_script($(this).attr("data-id"));

                });

                function load_script(id) {

                    idd = $("#sns" + id);

                    id = $("#grp" + id);

                    $(".smart_sns_box2 .box_contant").hide();

                    $(id).attr("class", "clk_circle active");

                    $(id).find("circle").attr("stroke", '#379BAE');

                    $(id).find("path").attr("fill", '#379BAE');

                    $(id).find("rect").attr("fill", '#379BAE');

                    $(id).find("polygon").attr("fill", '#379BAE');

                    $(idd).fadeIn();

                }

            });







        </script>



        <style type="text/css">

            .clk_circle.active circle{

                stroke: rgb(58, 167, 187);

            }

            clk_circle.active path{

                /* fill: ;*/

            }

            .smart_sns_box2 .box_contant{

                display: none;

            }

        </style>