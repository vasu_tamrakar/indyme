<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Indyme - Welcome</title>
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo  get_template_directory_uri(); ?>/images/indymefavicons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/indymefavicons/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">

<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700italic,700,300,100italic,100,300italic|Open+Sans:400,300,300italic,400italic,600,600italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Bootstrap -->
<link href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap.min.css" rel="stylesheet">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="<?php bloginfo('stylesheet_directory'); ?>/css/mystyle.css" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_directory'); ?>/css/responsive.css" rel="stylesheet">
<style>
.carousel-inner.onebyone-carosel {
	margin: auto;
	width: 90%;
}
.onebyone-carosel .active.left {
	left: -33.33%;
}
.onebyone-carosel .active.right {
	left: 33.33%;
}
.onebyone-carosel .next {
	left: 33.33%;
}
.onebyone-carosel .prev {
	left: -33.33%;
}

</style>

		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title><?php wp_title( '&#124;', true, 'right' ); ?></title>

		<link rel="profile" href="http://gmpg.org/xfn/11"/>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>
    <link href="<?php bloginfo('stylesheet_directory'); ?>/core/css/bootstrap.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_directory'); ?>/core/css/bootstrap-theme.css" rel="stylesheet">

		<?php wp_head(); ?>
                
	
</head>
<body>
<div class="container-fluid">
<?php if(is_page( 'spvm' )){ ?>
<div class="landing-nav">
	<div class="logobox">
		<div style="width: 185px;"><a href="<?php bloginfo('url'); ?>"><img src="<?php echo bloginfo('template_url') ?>/images/white-logo.png" /></a></div>
	</div>
	<ul class="nav-items">
	<li><i class="fa fa-phone"></i> +1.858.268.0717</li>
	<li><a href="<?php bloginfo('url'); ?>/#request" class="primary btn btn-success">REQUEST A DEMO</a></li>
	<div style="clear: both;"></div>
	</ul>
</div>
<?php } else { ?>
<header id="header" class="">
  <div class="desktop_header">
    <div class="headertop">
      <div class="top_info container">
        <div class="col-sm-3 col-md-3">
        </div>
        <ul class="col-sm-9 col-md-9">
          <li><a href="#"><i class="fa fa-phone"></i> +1.858.268.0717</a></li>
          <li><a href="https://www.linkedin.com/company/indyme-solutions">Connect with us: <i class="fa fa-linkedin-square"></i></a></li>
          <?php if(is_page( 'home' )){ ?>
          <li class="re_demo scroll_btn"><a href="<?php bloginfo('url'); ?>/#request">REQUEST A DEMO</a></li>
           <?php } else { ?>
          <li class="re_demo scroll_btn"><a href="<?php bloginfo('url'); ?>/?request">REQUEST A DEMO</a></li>
          <?php    } ?>
          <!--<li><a href="#">Language: <span class="flag"></span> English <i class="fa fa-angle-down"></i></a></li>-->
        </ul>
      </div>
    </div>
    <div class="headerbottom container">
      <div class="logobox">
        <div class="white-logo" style="width: 185px;"><a href="<?php bloginfo('url'); ?>"><img src="<?php echo bloginfo('template_url') ?>/images/white-logo.png" /></a></div>
        <div class="green-logo" style="width: 185px;"><a href="<?php bloginfo('url'); ?>"><img src="<?php echo bloginfo('template_url') ?>/images/indyme-logo.png" /></a></div>
      </div>
      <div class="navbox">
        <nav class="mainmenu">
	        <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
            <?php if(is_page( 'home' )){ ?>
          <div class="re_demo scroll_btn"><a href="<?php bloginfo('url'); ?>/#request">REQUEST A DEMO</a></div>
           <?php } else { ?>
          <div class="re_demo scroll_btn"><a href="<?php bloginfo('url'); ?>/?request">REQUEST A DEMO</a></div>
           <?php    } ?>
        </nav>
      </div>
    </div>
  </div>
  <div class="mobile_header">
    <div class="headertop">
      <div class="top_info">
        <div class="col-sm-3 col-md-3">
          
        </div>
        <ul class="col-sm-9 col-md-9">
          <li style="display:none;"><a href="#"><i class="fa fa-phone"></i> +1.858.268.0717</a></li>
          <li style="display:none;"><a href="#">Connect with us: <i class="fa fa-linkedin-square"></i></a></li>
          <li  style="display:none;"  class="re_demo"><a href="#">REQUEST A DEMO</a></li>
          <!--<li><a href="#">Language: <span class="flag"></span> English <i class="fa fa-angle-down"></i></a></li>-->
        </ul>
      </div>
    </div>
    <div class="headerbottom">
      <div class="logobox">
        <div class="white-logo" style="width: 185px;"><a href="<?php bloginfo('url'); ?>"><img src="<?php echo bloginfo('template_url') ?>/images/white-logo.png" /></a></div>
		<div class="green-logo" style="width: 185px;"><a href="<?php bloginfo('url'); ?>"><img src="<?php echo bloginfo('template_url') ?>/images/indyme-logo.png" /></a></div>

      </div>
      <div class="navbox">
        <div class="toggle" id="toggle"><i class="fa fa-bars"></i></div>
        <nav class="mainmenu">
        <div class="call_no"><a href="#"><i class="fa fa-phone"></i> +1.858.268.0717 </a></div>
		<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>

        </nav>
      </div>
    </div>
  </div>
</header>
<?php    } ?>
