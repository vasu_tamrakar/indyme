<?php  
/*
Template Name: Solution
  */
get_header(); ?>
<?php  if(have_posts()): while(have_posts()){ the_post();?>
<div class="comman_bnr_box" style=" background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>');">
  <div class="cbb_box">
    <div class="cbb_contant">
    <div class="container">
     <?php the_content(); ?>
      </div>
    </div>
  </div>
</div>
<div class="allcomman_box container">
  <div class="tbspace">
    <div class=""> 
      
      <div class="row">
        <div class="col-md-12"> 
          <!-- tabs left -->
          <div class="tabbable tabs-left">
            <ul class="nav nav-tabs desktop_tab">
             <?php
		                	$i=0; if( have_rows('solution-block') ):
                      $arr = array('customer', 'loss', 'fit', 'product', 'satisfaction', 'task-management', 'security', 'analytics');
		                	while ( have_rows('solution-block') ) : the_row();
		                ?>
						<li class="<?php if(isset($_GET['tab']) && $_GET['tab'] == $arr[$i]){echo 'active';}elseif(!isset($_GET['tab']) && $i == 0){echo ($i==0)?'active':'';} ?>" role="presentation" id="">
							<a class="scroll_btn_sol" href="#tab<?php echo  $i ?>" aria-controls="#tab<?php echo  $i++ ?>" role="tab" data-toggle="tab">
								<?php the_sub_field('service_icon') ?>
								<h3><?php the_sub_field('solution_name') ?></h3>
							</a>
						</li>
						<?php
			                endwhile;
			                endif ;
			            ?>
            </ul>
            
              <div class="tab-content scroll_div_sol">
                   <?php  $i=0; if( have_rows('solution-block') ){
                    
	                       while ( have_rows('solution-block') ) { the_row();  ?>
					<div role="tabpanel" class="tab-pane <?php if(isset($_GET['tab']) && $_GET['tab'] == $arr[$i]){echo 'active';}elseif(!isset($_GET['tab']) && $i == 0){echo ($i==0)?'active':'';} ?> fade in" id="tab<?php echo $i++ ?>">
						<div class="small-heading">
							<?php the_sub_field('solution_description') ?>
						</div>
					</div>
					<?php }} ?> 
                  
              </div>
          </div>
          
          <!-- /tabs --> 
          
        </div>
      </div>
      <!-- /row --> 
    </div>
    <hr>
  </div>
</div>
<div class="t_req_demo">
<div class="container">
  <?php if(get_field('request_a_demo_text')): ?>
    <div class="trd_l indybtn"><?php echo get_field('request_a_demo_text'); ?> <a href="<?php echo get_field('request_a_demo_link'); ?>" type="button" class="btn btn-success primary">REQUEST A DEMO </a>
  </div>
    <?php endif; ?>
  </div>
</div>
<?php } endif; ?>
<?php get_footer(); ?>
<script>
$(document).ready(function(){
    var str = window.location.href;
    var res = str.split("?");
    var lik = res[1];
   // alert(lik);
     if(lik == 'customer'){
       $('li#tab0 a').click();
     }else if(lik == 'loss'){
          $('li#tab1 a').trigger('click');
     }
     else if(lik == 'fit'){
          $('li#tab2 a').trigger('click');
     }
     else if(lik == 'product'){
          $('li#tab3 a').trigger('click');
     }
     else if(lik == 'satisfaction'){
          $('#tab4 a').click();
     }
      else if(lik == 'task-management'){
          $('#tab5 a').click();
     }
      else if(lik == 'security'){
          $('#tab6 a').click();
     }
       else if(lik == 'analytics'){
          $('#tab7 a').click();
     }else{
          $('#tab0 a').click();
     }
  });
  

    $(".scroll_btn_sol").click(function() {
      $('html,body').animate({scrollTop: $(".scroll_div_sol").offset().top - 75},'slow');
    });
 

</script>
