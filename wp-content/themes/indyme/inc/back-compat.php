<?php
/**
 * Indyme back compat functionality
 *
 * Prevents Indyme from running on WordPress versions prior to 3.6,
 * since this theme is not meant to be backward compatible beyond that
 * and relies on many newer functions and markup changes introduced in 3.6.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Indyme 1.0
 */

/**
 * Prevent switching to Indyme on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Indyme 1.0
 */
function indyme_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'indyme_upgrade_notice' );
}
add_action( 'after_switch_theme', 'indyme_switch_theme' );

/**
 * Add message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Indyme on WordPress versions prior to 3.6.
 *
 * @since Indyme 1.0
 */
function indyme_upgrade_notice() {
	$message = sprintf( __( 'Indyme requires at least WordPress version 3.6. You are running version %s. Please upgrade and try again.', 'indyme' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevent the Customizer from being loaded on WordPress versions prior to 3.6.
 *
 * @since Indyme 1.0
 */
function indyme_customize() {
	wp_die( sprintf( __( 'Indyme requires at least WordPress version 3.6. You are running version %s. Please upgrade and try again.', 'indyme' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'indyme_customize' );

/**
 * Prevent the Theme Preview from being loaded on WordPress versions prior to 3.4.
 *
 * @since Indyme 1.0
 */
function indyme_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Indyme requires at least WordPress version 3.6. You are running version %s. Please upgrade and try again.', 'indyme' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'indyme_preview' );
