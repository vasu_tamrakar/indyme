<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'indyme_data');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vOZ`6sQ#nz`+,dPkn,= %h&*7|T&Z-MK5N&MfM]CDSLNGu^% e6_%+(rO@0yDD{|');
define('SECURE_AUTH_KEY',  'c!%SW62~ODDCYI*1{_zeD}iJ~4_@LlINA8H#;V-1s$uSMwcH];7|f*KLO*-:4<0;');
define('LOGGED_IN_KEY',    '`@>S|bV%Y/=77q%JmC%AWz$p<[u2s0Qe/!LPUvG<A+=w?-$pe%+!sv{-R?FVC~II');
define('NONCE_KEY',        ']O+}Aht4cYxeZ!aX(QX 5tZP5<bhZ gzy5i5*`gzGD4k7m*[RDJ=%Ng-n[7%[=CK');
define('AUTH_SALT',        'xg|2MoOB1yyM$F?O?qk4}>#(E;F+Pk*KBM!E;-tn5O?Zzr(<d9MCr8[_w|7v{rvg');
define('SECURE_AUTH_SALT', '8axnG%+mU9ry!2/.qX!I7W1a+spY04u7<,{5+`3OI4;G=y>HMQ{Ua+<(+Y&0J55O');
define('LOGGED_IN_SALT',   'a@18INlg.0!Mj!x:oYtQmJ^+OwH+#,;zp]o5o:i9Zlxp9&<)`~H>q^li-3y{$E!G');
define('NONCE_SALT',       'x+,FIfcgk-Gj Pslr#VE(MlE(x8=yJ-mN%#eD]zq3:W3BAE-U@XSPbq.C,3MfY)|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'in_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
