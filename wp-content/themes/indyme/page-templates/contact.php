<?php
/*
 * Template Name: Contact
 */
get_header();?>
<style>

    .cbb_box{background: none;height: 440px;}

    #header{ background-color: rgba(0, 0, 0, 1);height: 160px;position: relative;}

    .desktop_header {background: #93b924;}

    .headerbottom {top: 85px;}

    .comman_bnr_box{height: 600px;}

    .top_info ul li.re_demo > a {

      border: 1px solid #000000;

      background: #000000;

      border-radius: 7px;

      display: block;

      padding: 10px 16px;

      -webkit-transition: all 0.5s ease-in-out;

      -moz-transition: all 0.5s ease-in-out;

      -o-transition: all 0.5s ease-in-out;

      transition: all 0.5s ease-in-out;

    }

    .desktop_header.fixed .top_info ul li.re_demo{margin-right: 40px;}

   .desktop_header.fixed .top_info ul li.re_demo a{padding: 3px 16px;}

    .top_info ul li.re_demo > a:hover, .top_info ul li.re_demo > a:focus {

      border: 1px solid #ffffff;

      background: none;

    }

    .top_info ul li.re_demo {padding: 0;border: 0;}

    @media only screen and (max-width: 767px) {

      #header{ height: auto;}

      .headerbottom {top: 5px;}

    }

</style>

<div class="allcomman_box d_page">

  <div class="tbspace container">

    <div class="col-lg-8">

      <h2 class="page_heading">Contact Us</h2>

      <div class="contact_us clearfix">

      <?php echo do_shortcode('[contact-form-7 id="56" title="Contact form 1"]');  ?>

      </div>

    </div>
      
    <div class="col-lg-4">

      <div class="sideBar">
<?php if(is_active_sidebar( 'sidebar-2' ) ){
                    dynamic_sidebar('sidebar-2');
                }
          ?>
         
      </div>

    </div>

  </div>

  <div id="find_reseller" class="row find-sell scroll_div"><div class="col-lg-12"><?php  echo do_shortcode('[wpsl]');  ?></div></div>

</div>





 <?php get_footer(); ?>

<script>

    $(".wpsl-provided-by").css('display','none');

    $(".wpsl-store-below #wpsl-result-list").css('display','none');

    $( "#wpsl-search-btn" ).click(function() {

    $(".wpsl-store-below #wpsl-result-list").css('display','block');

setTimeout(

  function() 

  {

    var ct = $('div#wpsl-stores li').length;

    var vgt = "Results";

    var content = "<span class='titles'>Indyme's worldwide reseller network provides customers with local support and service for best-of-breed Smart Response &reg;retail technology solutions.</span>";

    var ctr = +ct+" "+vgt+" "+content;

    if($('li.no-results').length === 1){

    $('#resultc').html("");

    }else{

      $('#resultc').html(ctr);   

    }

  }, 1000);

 

});

</script>

<script type="text/javascript">



function stopRKey(evt) {

  var evt = (evt) ? evt : ((event) ? event : null);

  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);

  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}

}



document.onkeypress = stopRKey;



</script> 



